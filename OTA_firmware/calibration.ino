//void reset_offsets()
//{
//  uint8_t tmp[] = {0, 0};
//  while (i2cWrite(0x06, tmp, 2, false)); // XAxis offset, bytes, set 0
//  while (i2cWrite(0x08, tmp, 2, false)); // YAxis offset, bytes, set 0
//  while (i2cWrite(0x0A, tmp, 2, false)); // ZAxis offset, bytes, set 0
//  while (i2cWrite(0x13, tmp, 2, false)); // XGyro offset, bytes, set 0
//  while (i2cWrite(0x15, tmp, 2, false)); // YGyro offset, bytes, set 0
//  while (i2cWrite(0x17, tmp, 2, false)); // ZGyro offset, bytes, set 0
//}
//
//void calibration_merge(int key) //This function will merge the offset in the memory with that of the hinge
//{
//  if ((key == 180) || (key == 270))
//  {
//    calibration(key);
//    write_calibration(list);
//    return;
//  }
//  else
//  {
//    int offsets_tmp[6];
//    calibration(180);
//    offsets_tmp[0] = list[0] + list[1] * 256;
//    offsets_tmp[1] = list[2] + list[3] * 256;
//    offsets_tmp[2] = list[6] + list[5] * 256;
//    //write_calibration(list);
//    Serial.print("Offsets: ");
//    Serial.print(offsets_tmp[0]); Serial.println(" ");
//    Serial.print(offsets_tmp[1]); Serial.println(" ");
//    Serial.print(offsets_tmp[2]); Serial.println(" ");
//    while (!Serial.available()) {}
//
//    if (Serial.parseInt() == 0)
//    {
//      calibration(270);
//      //write_calibration(list);
//      Serial.print("Offsets: ");
//      Serial.print(offsets_tmp[0]); Serial.println(" ");
//      Serial.print(offsets_tmp[1]); Serial.println(" ");
//      Serial.print(offsets_tmp[2]); Serial.println(" ");
//      offsets_tmp[0] = (offsets_tmp[0] + list[0] + list[1] * 256) / 2;
//      offsets_tmp[1] = (offsets_tmp[1] + list[2] + list[3] * 256) / 2;
//      offsets_tmp[2] = (offsets_tmp[2] + list[6] + list[5] * 256) / 2;
//      list[0] = highByte(offsets_tmp[0]);
//      list[1] = lowByte(offsets_tmp[0]);
//      list[2] = highByte(offsets_tmp[1]);
//      list[3] = lowByte(offsets_tmp[1]);
//      list[4] = highByte(offsets_tmp[2]);
//      list[5] = lowByte(offsets_tmp[2]);
//      write_calibration(list);
//    }
//    else return;
//  }
//
//}
//
//int process_180()//This will calibrate the offsets towards 180 degrees
//{
//  int count = 0;
//  if (abs(mean_accX) <= accel_threshold) count++;
//  else accX_offset = accX_offset - mean_accX / accel_threshold;
//
//  //if (abs(16384 - mean_accY) <= accel_threshold) done_flag++;
//  //else accY_offset = accY_offset + (16384 - mean_accY) / accel_threshold;
//  if (abs(mean_accY) <= accel_threshold) count++;
//  else accY_offset = accY_offset - mean_accY / accel_threshold;
//
//  //if (abs(mean_accZ) <= accel_threshold) done_flag++;
//  //else accZ_offset = accZ_offset - mean_accZ / accel_threshold;
//
//  if (abs(16384 - mean_accZ) <= accel_threshold) count++;
//  else accZ_offset = accZ_offset + (16384 - mean_accZ) / accel_threshold;
//
//  if (abs(mean_gyroX) <= gyro_threshold) count++;
//  else gyroX_offset = gyroX_offset - mean_gyroX / (gyro_threshold + 1);
//
//  if (abs(mean_gyroY) <= gyro_threshold) count++;
//  else gyroY_offset = gyroY_offset - mean_gyroY / (gyro_threshold + 1);
//
//  if (abs(mean_gyroZ) <= gyro_threshold) count++;
//  else gyroZ_offset = gyroZ_offset - mean_gyroZ / (gyro_threshold + 1);
//
//  return count;
//}
//
//int process_270()//This will calibrate the offsets towards 270 degrees
//{
//  int count = 0;
//  if (abs(mean_accX) <= accel_threshold) count++;
//  else accX_offset = accX_offset - mean_accX / accel_threshold;
//
//  if (abs(16384 - mean_accY) <= accel_threshold) count++;
//  else accY_offset = accY_offset + (16384 - mean_accY) / accel_threshold;
//  //if (abs(mean_accY) <= accel_threshold) count++;
//  //else accY_offset = accY_offset - mean_accY / accel_threshold;
//
//  if (abs(mean_accZ) <= accel_threshold) count++;
//  else accZ_offset = accZ_offset - mean_accZ / accel_threshold;
//
//  //if (abs(16384-mean_accZ) <= accel_threshold) count++;
//  //else accZ_offset = accZ_offset + (16384 - mean_accZ) / accel_threshold;
//
//  if (abs(mean_gyroX) <= gyro_threshold) count++;
//  else gyroX_offset = gyroX_offset - mean_gyroX / (gyro_threshold + 1);
//
//  if (abs(mean_gyroY) <= gyro_threshold) count++;
//  else gyroY_offset = gyroY_offset - mean_gyroY / (gyro_threshold + 1);
//
//  if (abs(mean_gyroZ) <= gyro_threshold) count++;
//  else gyroZ_offset = gyroZ_offset - mean_gyroZ / (gyro_threshold + 1);
//  
//  return count;
//}
//
//void calibration(int cal_mode) {
//  uint8_t tmp[] = {0, 0};
//
//  accX_offset = -mean_accX / 8;
//
//  if (cal_mode == 180)
//  {
//    accY_offset = -mean_accY / 8;
//    accZ_offset = (16384 - mean_accZ) / 8;
//  }
//
//  if (cal_mode == 270)
//  {
//    accY_offset = (16384 - mean_accY) / 8;
//    accZ_offset =  -mean_accZ;
//  }
//
//  gyroX_offset = -mean_gyroX / 4;
//  gyroY_offset = -mean_gyroY / 4;
//  gyroZ_offset = -mean_gyroZ / 4;
//
//  while (1) {
//    int done_flag = 0;
//    tmp[0] = highByte(accX_offset);
//    tmp[1] = lowByte(accX_offset);
//    while (i2cWrite(0x06, tmp, 2, false)); // XAxis offset, bytes
//    tmp[0] = highByte(accY_offset);
//    tmp[1] = lowByte(accY_offset);
//    while (i2cWrite(0x08, tmp, 2, false)); // YAxis offset, bytes
//    tmp[0] = highByte(accZ_offset);
//    tmp[1] = lowByte(accZ_offset);
//    while (i2cWrite(0x0A, tmp, 2, false)); // ZAxis offset, bytes
//    tmp[0] = highByte(gyroX_offset);
//    tmp[1] = lowByte(gyroX_offset);
//    while (i2cWrite(0x13, tmp, 2, false)); // XGyro offset, bytes
//    tmp[0] = highByte(gyroY_offset);
//    tmp[1] = lowByte(gyroY_offset);
//    while (i2cWrite(0x15, tmp, 2, false)); // YGyro offset, bytes
//    tmp[0] = highByte(gyroZ_offset);
//    tmp[1] = lowByte(gyroZ_offset);
//    while (i2cWrite(0x17, tmp, 2, true)); // ZGyro offset, bytes
//
//    /*Serial.print("Values: \t");*/
//    Serial.print(mean_accX);
//    Serial.print("\t");
//    Serial.print(mean_accY);
//    Serial.print("\t");
//    Serial.print(mean_accZ);
//    Serial.print("\t");
//    Serial.print(mean_gyroX);
//    Serial.print("\t");
//    Serial.print(mean_gyroY);
//    Serial.print("\t");
//    Serial.println(mean_gyroZ);
//    /*Serial.print("Offsets: \t");*/
//    /*Serial.print(accX_offset);
//    Serial.print("\t");
//    Serial.print(accY_offset);
//    Serial.print("\t");
//    Serial.print(accZ_offset);
//    Serial.print("\t");
//    Serial.print(gyroX_offset);
//    Serial.print("\t");
//    Serial.print(gyroY_offset);
//    Serial.print("\t");
//    Serial.println(gyroZ_offset);*/
//
//    get_average();
//
//    if (cal_mode == 180)
//    {
//      done_flag = process_180();
//    }
//
//    if (cal_mode == 270)
//    {
//      done_flag = process_270();
//    }
//
//    if (done_flag == 6)
//    {
//      list[0] = highByte(accX_offset);
//      list[1] = lowByte(accX_offset);
//      list[2] = highByte(accY_offset);
//      list[3] = lowByte(accY_offset);
//      list[4] = highByte(accZ_offset);
//      list[5] = lowByte(accZ_offset);
//      list[6] = highByte(gyroX_offset);
//      list[7] = lowByte(gyroX_offset);
//      list[8] = highByte(gyroY_offset);
//      list[9] = lowByte(gyroY_offset);
//      list[10] = highByte(gyroZ_offset);
//      list[11] = lowByte(gyroZ_offset);
//
//      Serial.print("Calibration done - ");
//      Serial.print(cal_mode);
//      Serial.println("deg.");
//      break;
//    }
//  }
//}
//
//void get_average() {
//  long i = 0;
//  long buff_accX = 0, buff_accY = 0, buff_accZ = 0; //These are for summing up the readings for averaging
//  long buff_gyroX = 0, buff_gyroY = 0, buff_gyroZ = 0; //These are for summing up the readings for averaging
//  int16_t tmpaX, tmpaY, tmpaZ, tmpgX, tmpgY, tmpgZ;
//  while (i < (cal_buffer + 76)) { //Discard the first 75 measurememnts
//    // read sensors
//    while (i2cRead(0x3B, i2cData, 14));
//    tmpaX = ((i2cData[0] << 8) | i2cData[1]);
//    tmpaY = ((i2cData[2] << 8) | i2cData[3]);
//    tmpaZ = ((i2cData[4] << 8) | i2cData[5]);
//    tempRaw = (i2cData[6] << 8) | i2cData[7];
//    tmpgX = (i2cData[8] << 8) | i2cData[9];
//    tmpgY = (i2cData[10] << 8) | i2cData[11];
//    tmpgZ = (i2cData[12] << 8) | i2cData[13];
//
//    if (i > 75 && i <= (cal_buffer + 75)) { //Discard the first 75 measurememnts
//      buff_accX = buff_accX + tmpaX;
//      buff_accY = buff_accY + tmpaY;
//      buff_accZ = buff_accZ + tmpaZ;
//      buff_gyroX = buff_gyroX + tmpgX;
//      buff_gyroY = buff_gyroY + tmpgY;
//      buff_gyroZ = buff_gyroZ + tmpgZ;
//    }
//    if (i == (cal_buffer + 75)) { //Record the average, when done calibrating
//      mean_accX = buff_accX / cal_buffer;
//      mean_accY = buff_accY / cal_buffer;
//      mean_accZ = buff_accZ / cal_buffer;
//      mean_gyroX = buff_gyroX / cal_buffer;
//      mean_gyroY = buff_gyroY / cal_buffer;
//      mean_gyroZ = buff_gyroZ / cal_buffer;
//    }
//    i++;
//    delay(3);
//  }
//}
//
///*--------------------- USE Internal EEPROM (1kb) for storing calibration values -------------------------------*/
//void read_calibration()
//{
//  char tmpa[12];//12 offsets with ints
//  for (unsigned int k = 0; k < 12; k++)
//    tmpa[k] = EEPROM.read(k);
//
//  for (unsigned int h = 0; h < 6; h++)
//    Serial.println(tmpa[h * 2] * 256 + tmpa[h * 2 + 1]);
//
//  /*for (unsigned char i = 0; i < 12; i++)
//    calibration_values[i] = tmpa[2 * i] * 256 + tmpa[2 * i + 1];*/
//  //Write the values to the IMU from the On Chip EEPROM
//  unsigned char tmp[2];
//  int counter = 0;
//  tmp[0] = tmpa[counter];
//  tmp[1] = tmpa[counter + 1];
//  counter += 2;
//  while (i2cWrite(0x06, tmp, 2, false)); // XAxis offset, bytes
//  tmp[0] = tmpa[counter];
//  tmp[1] = tmpa[counter + 1];
//  counter += 2;
//  while (i2cWrite(0x08, tmp, 2, false)); // YAxis offset, bytes
//  tmp[0] = tmpa[counter];
//  tmp[1] = tmpa[counter + 1];
//  counter += 2;
//  while (i2cWrite(0x0A, tmp, 2, false)); // ZAxis offset, bytes
//  tmp[0] = tmpa[counter];
//  tmp[1] = tmpa[counter + 1];
//  counter += 2;
//  while (i2cWrite(0x13, tmp, 2, false)); // XGyro offset, bytes
//  tmp[0] = tmpa[counter];
//  tmp[1] = tmpa[counter + 1];
//  counter += 2;
//  while (i2cWrite(0x15, tmp, 2, false)); // YGyro offset, bytes
//  tmp[0] = tmpa[counter];
//  tmp[1] = tmpa[counter + 1];
//  counter += 2;
//  while (i2cWrite(0x17, tmp, 2, true)); // ZGyro offset, bytes
//}
//
//void write_calibration(unsigned char* tmp)
//{
//  //Serial.print("Writing calibration values ... ");
//  /*char tmp[24];//Bytes to be written from int array
//  for (unsigned char g = 0; g < 12; g++)
//  {
//    tmp[g * 2] = input[g] / 256; //Most significant bytes first
//    tmp[g * 2 + 1] = input[g] % 256; //Least significant bytes first
//  }*/
//  for (int k = 0; k < 24; k++)
//    EEPROM.write(k, tmp[k]);
//
//  //read_calibration();//Update the values for the device
//  //Serial.print("DONE.");
//}
//
//
//
//
//

