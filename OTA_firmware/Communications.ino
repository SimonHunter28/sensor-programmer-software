void Serial_check()
{
  //These are the input buffers
  char comm[16][32];
  unsigned int comm_i = 0;
  unsigned int string_count = 0;
  delay(10);//To avoid assynchronous issues

  while (Serial.available() > 0)
  {
    delay(1);//To avoid asynchronous issues
    char inChar = (char)Serial.read();
    comm[string_count][comm_i] = inChar;
    comm_i++;

    if ((inChar == ' ') & (string_count < 16)) { //space is a delimiter
      comm[string_count][comm_i] = '\0';
      string_count++;
      comm_i = 0;
    }

    if (inChar == '@') //stream
    {
      string_count = 0;
      comm_i = 0;

      digitalWrite(RLED, LOW);
      digitalWrite(GLED, LOW);
      digitalWrite(BLED, HIGH);

      printData();
      streaming=1;

      while (Serial.available())
        Serial.read();
    }

    if (inChar == '+') //calibrate, the two numbers are thresholds
    {
      string_count = 0;
      comm_i = 0;
      read_calibration();
      //accel_threshold = atoi(comm[0]);
      //gyro_threshold = atoi(comm[1]);
      //calibration_flag = atoi(comm[2]);
      calibrate2();

      while (Serial.available())
        Serial.read();
    }
    
    if (inChar == '?') //start OTA
    {
      string_count = 0;
      comm_i = 0;

      OTA_flag = 1;

      digitalWrite(RLED, LOW);
      digitalWrite(GLED, LOW);
      digitalWrite(BLED, HIGH);

      while (Serial.available() > 0) //Clean the buffer
        Serial.read();
    }

    if (inChar == '$') //set BT
    {
      string_count = 0;
      comm_i = 0;
      
      digitalWrite(RLED, HIGH);
      digitalWrite(GLED, LOW);
      digitalWrite(BLED, LOW);

      sprintf(BT_name, "SN,%s", comm[0]);
      BT_init();

      /*for (unsigned int j = 0; j < BT_resp_idx; j++)
        Serial.write(BT_responses[j]);
      BT_resp_idx = 0;*/

      while (Serial.available() > 0) //Clean the buffer
        Serial.read();
    }

    /*if (inChar == '>') //write calibration values
    {
      string_count = 0;
      comm_i = 0;

      int tmp[12];
      for (unsigned char g = 0; g < 12; g++)
        tmp[g] = atoi(comm[g]);

      //write_calibration(tmp);

      digitalWrite(RLED, LOW);
      digitalWrite(GLED, LOW);
      digitalWrite(BLED, LOW);
      while (Serial.available() > 0) //Clean the buffer
        Serial.read();
    }

    if (inChar == '^') //initialize memory
    {
      string_count = 0;
      comm_i = 0;
      digitalWrite(RLED, HIGH);
      digitalWrite(GLED, LOW);
      digitalWrite(BLED, HIGH);
      /*int tmp[]={1, 2, 3, 4, 5, 6, 7, 8};
      for(long k=0; k<50000; k+=16)
        write_page(k, tmp);


      while (Serial.available() > 0) //Clean the buffer
        Serial.read();
    }

    if (inChar == '<') //read calibration memory
    {
      string_count = 0;
      comm_i = 0;

      digitalWrite(RLED, LOW);
      digitalWrite(GLED, HIGH);
      digitalWrite(BLED, LOW);
      for (long k = 0; k < 70000; k += 256)
      {
        Serial.println("Mem block: ");
        Serial.println(k / 256);
        dump_16(k);
      }
      for (unsigned int g = 0; g < 12; g++)
        Serial.println(calibration_values[g]);

      while (Serial.available() > 0) //Clean the buffer
        Serial.read();
    }

    if (inChar == '*') //erase memory
    {
      string_count = 0;
      comm_i = 0;
      digitalWrite(RLED, LOW);
      digitalWrite(GLED, HIGH);
      digitalWrite(BLED, HIGH);

      erase_flash(atoi(comm[0]));
      while (Serial.available() > 0) //Clean the buffer
        Serial.read();
    }*/
  }
  
  while (Serial1.available() > 0)
  {
    delay(1);//To avoid asynchronous issues
    char inChar = (char)Serial1.read();
    comm[string_count][comm_i] = inChar;
    comm_i++;

    if ((inChar == ' ') & (string_count < 16)) { //space is a delimiter
      comm[string_count][comm_i] = '\0';
      string_count++;
      comm_i = 0;
    }

    if (inChar == '0') //start OTA
    {
      string_count = 0;
      comm_i = 0;

      OTA_flag=1;
      
      digitalWrite(RLED, LOW);
      digitalWrite(GLED, LOW);
      digitalWrite(BLED, HIGH);

      while (Serial1.available() > 0) //Clean the buffer
        Serial1.read();
    }

  }
}

void printData()
{
  Serial.print("D ");
  Serial.print(accX);
  Serial.print(" ");
  Serial.print(accY);
  Serial.print(" ");
  Serial.print(accZ);
  Serial.print(" ");
  Serial.print(gyroX);
  Serial.print(" ");
  Serial.print(gyroY);
  Serial.print(" ");
  Serial.print(gyroZ);
  Serial.print(" ");
  Serial.print(magX);
  Serial.print(" ");
  Serial.print(magY);
  Serial.print(" ");
  Serial.print(magZ);
  Serial.println("X");
}


