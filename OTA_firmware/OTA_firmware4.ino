/* This is test firmware, which calibrates IMUs and sets the bluetooth name, OTA is also available*/
/*Calibration is done by fitting measured average values to two known points and recording the slope and coef*/
/*y(actual)=a*x(measured)+b, where the formula and coefficients will remain the same for calculating real values*/

/*Written by Karolis, August 2015, Marblar ltd.*/

#include <Wire.h>
#include <avr/wdt.h>

/*Testing on board firmware*/
#include <EEPROM.h>
int address = 0;
byte value;
int y_val_av1=0;
int z_val_av1=0;
int y_val_av2=0;
int z_val_av2=0;

int cal_point1[3]={0, 0, -16384};//These are the actual points, x, y, z, 180deg.
int cal_point2[3]={0, 16384, 0};//These are the actual points, x, y, z, 270 deg;
/*--------------------*/

#define BT_BAUD 115200//Current baud rate, which will be set to 115200
char BT_name[32];//The command, which changes the name of the BT module
char BT_responses[1024];
int BT_resp_idx = 0;

/*Memory stuff*/
#include <SPIFlash.h>    //get it here: https://github.com/LowPowerLab/SPIFlash
#include <SPI.h>
#define FLASH_SS      SS // and FLASH SS on D23
char input = 0;
long lastPeriod = -1;
SPIFlash flash(FLASH_SS, 0x0140);
long current_address = 0;
/*

/* Define some LED stuff*/
#define LED_ON 0 //Don't forget the LEDs are inverted, 0 turns them on.
#define LED_OFF 255

int RLED = A0;
int GLED = A1;
int BLED = A2;

int BATT_LEV = A4;
int CHG_STAT = A5;

/* IMU Data */
int cal_status = 0;
int calibration_flag = 0;
int16_t accX, accY, accZ;
int16_t gyroX, gyroY, gyroZ;//MPU-6050 variables
int16_t magX, magY, magZ;//MPU-6050 variables
int mean_accX, mean_accY, mean_accZ; //MPU-6050 calibration variables
int mean_gyroX, mean_gyroY, mean_gyroZ;
int mean_magX, mean_magY, mean_magZ;
int accX_offset, accY_offset, accZ_offset;
int gyroX_offset, gyroY_offset, gyroZ_offset; //MPU-6050 calibration variables
int magX_offset, magY_offset, magZ_offset; //MPU-6050 calibration variables
int cal_buffer = 300;   //Buffer for averaging for calibration
int accel_threshold = 8;   //Maximum Accl error for calibration
int gyro_threshold = 1;   //Maximum Gyro error for calibration
unsigned char list[12];

#define    MPU9250_ADDRESS            0x68
#define    MAG_ADDRESS                0x0C

#define    GYRO_FULL_SCALE_250_DPS    0x00  
#define    GYRO_FULL_SCALE_500_DPS    0x08
#define    GYRO_FULL_SCALE_1000_DPS   0x10
#define    GYRO_FULL_SCALE_2000_DPS   0x18

#define    ACC_FULL_SCALE_2_G        0x00  
#define    ACC_FULL_SCALE_4_G        0x08
#define    ACC_FULL_SCALE_8_G        0x10
#define    ACC_FULL_SCALE_16_G       0x18


volatile bool intFlag=false;
double ax, ay, az, gx, gy, gz, mx, my, mz;

uint8_t i2cData[14]; // Buffer for I2C data
uint32_t timer;
int16_t tempRaw;
int calibration_values[12]={0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0};
uint8_t calibration[24];

/* Device State*/
int devicestate = 1; //
int state;
int standby; //0: ACTIVE 1: DISABLED
double time;
double flashtimer;
double checktime;
String val;
double temp;

boolean streaming = false;

int OTA_flag=0;

void loop()
{
    digitalWrite(RLED, HIGH);
  digitalWrite(GLED, LOW);
  digitalWrite(BLED, LOW);
  delay(10);


  if ((Serial.available()||Serial1.available()))
    Serial_check();//Comes from the comms.h library, checks for input in the format: "number, number..... letter".
//
//    if(OTA_flag)
//    if(!OTA())
//      OTA_flag=0;

  /* Update all the values */
  
  while (i2cRead(0x3B, i2cData, 14));
  accX = ((i2cData[0] << 8) | i2cData[1]);
  accY = ((i2cData[2] << 8) | i2cData[3]);
  accZ = ((i2cData[4] << 8) | i2cData[5]);
  tempRaw = (i2cData[6] << 8) | i2cData[7];
  gyroX = (i2cData[8] << 8) | i2cData[9];
  gyroY = (i2cData[10] << 8) | i2cData[11];
  gyroZ = (i2cData[12] << 8) | i2cData[13];
  unsigned char ST1;
  do
  {
      I2Cread(MAG_ADDRESS,0x02,1,&ST1);
  }
  while (!(ST1&0x01));
  // Read magnetometer data
  uint8_t Mag[7];  
  I2Cread(MAG_ADDRESS,0x03,7,Mag);
  magX=-(Mag[1]<<8 | Mag[0]);
  magY=-(Mag[3]<<8 | Mag[2]);
  magZ=-(Mag[5]<<8 | Mag[4]);

  apply_calibration();
  //log_data();

  if (streaming)
  {
    printData();
  }

//  if (calibration_flag>0)
//  {
//    
//    digitalWrite(RLED, HIGH);
//    digitalWrite(GLED, LOW);
//    digitalWrite(BLED, HIGH);
//    Serial.println("Reset offsets");
//    reset_offsets();
//    Serial.println("Auto calibration started");
//    calibrate2();
//    /*get_average();//Start by recording the first values
//    calibration(calibration_flag);//This will perform autocal, based on flat surface*/
//    calibration_flag = 0;
//  }

}




