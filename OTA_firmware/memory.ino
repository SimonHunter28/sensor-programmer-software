/*--------------------- USE Internal EEPROM (1kb) for storing calibration values -------------------------------*/
/*void read_calibration()
{
  char tmp[24];
  for(unsigned int k=0; k<24; k++)
    tmp[k]=EEPROM.read(k);

  for(unsigned char i=0; i<12; i++)
    calibration_values[i]=tmp[2*i]*256+tmp[2*i+1];
}

void write_calibration(int* input)
{
  //Serial.print("Writing calibration values ... ");
  char tmp[24];//Bytes to be written from int array
  for(unsigned char g=0; g<12; g++)
  {
    tmp[g*2]=input[g]/256;//Most significant bytes first
    tmp[g*2+1]=input[g]%256;//Least significant bytes first
  }

  for (int k = 0; k < 24; k++)
    EEPROM.write(k, tmp[k]);

  read_calibration();//Update the values for the device
  //Serial.print("DONE.");
}*/

/*--------------------- USE External Flash (64Mb) for logging data -------------------------------*/

//Organisation of one page:
//2bytes X - Accel
//2bytes Y - Accel
//2bytes Z - Accel
//2bytes X - Gyro
//2bytes Y - Gyro
//2bytes Z - Gyro
//4bytes T - runtime in milliseconds
/*--------------------------------------------- Total page 16 bytes ------------------------------*/

/*void dump_16(long address) //d=dump flash area of 16 pages (16 bytes each)
{
  int length=16*16;
  char tmp[length];//read buffer
  char tmps[64];//Serial print buffer
  flash.readBytes(address, tmp, length);
  for(unsigned char k=0; k<length/16; k++)
  {
    sprintf(tmps, "D%d %d %d %d %d %dX %d %d", tmp[16*k]*256+tmp[16*k+1], tmp[16*k+2]*256+tmp[16*k+3], 
    tmp[16*k+4]*256+tmp[16*k+5], tmp[16*k+6]*256+tmp[16*k+7], tmp[16*k+8]*256+tmp[16*k+9], tmp[16*k+10]*256+tmp[16*k+11], tmp[16*k+12]*256+tmp[16*k+13], tmp[16*k+14]*256+tmp[16*k+15]);
    Serial.println(tmps);
  }
}

void erase_flash(long address)
{   
  Serial.print("Erasing Flash chip, starting address: ");
  Serial.println(address);
  flash.chipErase();
  while(flash.busy())
  {
    Serial.print(".");
    delay(400);
  }
  Serial.println("DONE"); 
}

void clean_flash()
{
   //Serial.print("DeviceID: ");
   //Serial.println(flash.readDeviceId(), HEX);

  for(long k=0; k<64000000; k+=32769)
  {
    Serial.print("Memory 32768 block: ");
    Serial.println(k);
    Serial.print("Read elements: ");
    Serial.print(flash.readByte(long(32768)*k));
    Serial.print(" ");
    Serial.print(flash.readByte(long(32768)*k+long(1)));
    Serial.print(" ");
    Serial.println(flash.readByte(long(32768)*k+long(2)));
    while(flash.busy());
    if((flash.readByte(long(32768)*k)==0xFF)&(flash.readByte(long(32768)*k+long(1))==0xFF)&(flash.readByte(long(32768)*k+long(2))==0xFF))
      break;
    else
      flash.blockErase32K(k);
  }
  current_address=0;
}

void write_page(long address, int *input)
{
  //Serial.print("Writing calibration values ... ");
  char tmp[16];//Bytes to be written from int array
  for(unsigned char g=0; g<8; g++)
  {
    tmp[g*2]=input[g]/256;//Most significant bytes first
    tmp[g*2+1]=input[g]%256;//Least significant bytes first
  }
  while(flash.busy());
  flash.writeBytes(address, tmp, 16);

}

void flash_init()
{
  if (flash.initialize())
    Serial.println("Init OK!");
  else
    Serial.println("Init FAIL!");
}

void log_data()
{
  int tmp[8];
  tmp[0]=accX;
  tmp[1]=accY;
  tmp[2]=accZ;
  tmp[3]=gyroX;
  tmp[4]=gyroY;
  tmp[5]=gyroZ;
  tmp[6]=millis()/65536;
  tmp[7]=millis()%65536;
  write_page(current_address, tmp);
  current_address+=16;
}
*/





