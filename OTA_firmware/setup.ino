// This function read Nbytes bytes from I2C device at address Address. 
// Put read bytes starting at register Register in the Data array. 
void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.endTransmission();
 
  // Read Nbytes
  Wire.requestFrom(Address, Nbytes); 
  uint8_t index=0;
  while (Wire.available())
    Data[index++]=Wire.read();
}
 
 
// Write a byte (Data) in device (Address) at register (Register)
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.write(Data);
  Wire.endTransmission();
}
 
void setup() {

  /*LED initialization*/
  pinMode(GLED, OUTPUT);
  pinMode(RLED, OUTPUT);
  pinMode(BLED, OUTPUT);
  //flash_init();
  //read_calibration();
  /*Serial1 communication and I2C initialization*/
  Serial.begin(115200);//This is for the USB interface
  
  Serial1.begin(115200);//This is for the BT interface
  Wire.begin();

  streaming=false;
  calibration_flag=false;
  /*Setup the IMU for data acquisition*/

/*  Wire.beginTransmission(MPU);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  */

  I2CwriteByte(MPU9250_ADDRESS,29,0x06);
  // Set gyroscope low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS,26,0x06);
  // Configure gyroscope range
  I2CwriteByte(MPU9250_ADDRESS,27,GYRO_FULL_SCALE_250_DPS);
  // Configure accelerometers range
  I2CwriteByte(MPU9250_ADDRESS,28,ACC_FULL_SCALE_2_G);
  // Set by pass mode for the magnetometers
  I2CwriteByte(MPU9250_ADDRESS,0x37,0x02);
  delay(100);
  I2CwriteByte(MAG_ADDRESS,0x0A,0x16);// Request continuous magnetometer measurements in 16 bits
  
  //TWBR = ((F_CPU / 400000L) - 16) / 2; // Set I2C frequency to 400kHz

  /*i2cData[0] = 7; // Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
  i2cData[1] = 0x00; // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
  i2cData[2] = 0x00; // Set Gyro Full Scale Range to ±250deg/s
  i2cData[3] = 0x00; // Set Accelerometer Full Scale Range to ±2g
  while (i2cWrite(0x19, i2cData, 4, false)); // Write to all four registers at once
  while (i2cWrite(0x6B, 0x01, true)); // PLL with X axis gyroscope reference and disable sleep mode

    while (i2cRead(0x75, i2cData, 1));
  if (i2cData[0] != 0x68) { // Read "WHO_AM_I" register
    Serial.print(F("Error reading sensor"));
    while (1);
  }
  delay(1000);*/

  //clean_flash();
  //unsigned char tmp[]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  //write_calibration(tmp, 24);
  //read_calibration();

}

void BT_init()
{
  /*Bluetooth initialisation procedure*/
  Serial1.begin(BT_BAUD);//This is for the Bluetooth interface
  delay(500);

  while(Serial1.available())//Flush the Serial1 buffer
    Serial1.read();

  BT_resp_idx=0;

  Serial1.print("$$$");
  /*Serial1.print("$");
  Serial1.print("$");*/

  while(Serial1.available()<1);//Wait for response "CMD"
  delay(100);//Let all message arrive
  while(Serial1.available())//Record the responses into a variable
  {
    BT_responses[BT_resp_idx]=Serial1.read();
    BT_resp_idx++;
  }
  Serial1.println("SF,1");//Factory reset
  
  while(Serial1.available()<1);//Wait for response "AOK"
  delay(100);//Let all message arrive
  
  while(Serial1.available())//Record the responses into a variable
  {
    BT_responses[BT_resp_idx]=Serial1.read();
    BT_resp_idx++;
  }
  
  Serial1.println(BT_name); // Change BT module name
  
  while(Serial1.available()<1);//Wait for response "AOK"
  delay(100);//Let all message arrive

  while(Serial1.available())//Record the responses into a variable
  {
    BT_responses[BT_resp_idx]=Serial1.read();
    BT_resp_idx++;
  }

  Serial1.println("SU,11");//Set baud to 115200

  while(Serial1.available()<1);//Wait for response "AOK"
  delay(100);//Let all message arrive

  while(Serial1.available())//Record the responses into a variable
  {
    BT_responses[BT_resp_idx]=Serial1.read();
    BT_resp_idx++;
  }

  Serial1.println("---");//Exit BT configuration
  //Serial1.println("Z");//Turn off the device
  Serial.println(BT_name); // Change BT module name
  delay(100);
    while(Serial1.available())//Record the responses into a variable
  {
    BT_responses[BT_resp_idx]=Serial1.read();
    BT_resp_idx++;
  }
}

