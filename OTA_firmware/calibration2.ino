void write_calibration(unsigned char* tmp, int lgth)
{
  for (int k = 0; k < lgth; k++)
    EEPROM.write(k, tmp[k]);
}

void reset_offsets()
{
  uint8_t tmp[] = {
    0, 0  };
  while (i2cWrite(0x06, tmp, 2, false)); // XAxis offset, bytes, set 0
  while (i2cWrite(0x08, tmp, 2, false)); // YAxis offset, bytes, set 0
  while (i2cWrite(0x0A, tmp, 2, false)); // ZAxis offset, bytes, set 0
  while (i2cWrite(0x13, tmp, 2, false)); // XGyro offset, bytes, set 0
  while (i2cWrite(0x15, tmp, 2, false)); // YGyro offset, bytes, set 0
  while (i2cWrite(0x17, tmp, 2, false)); // ZGyro offset, bytes, set 0
}

/*--------------------- USE Internal EEPROM (1kb) for storing calibration values -------------------------------*/
void read_calibration()
{
  unsigned char tmpa[24];//24 values, offsets and slopes
  for (unsigned int k = 0; k < 24; k++)
    tmpa[k] = EEPROM.read(k);

  /*for (unsigned int h = 0; h < 6; h++)
   Serial.println(tmpa[h * 2] * 256 + tmpa[h * 2 + 1]);*/

  for (unsigned char i = 0; i < 12; i++)
    calibration_values[i] = tmpa[2 * i] * 256 + tmpa[2 * i + 1];
  //Write the values to the IMU from the On Chip EEPROM
  /*unsigned char tmp[2];
   int counter = 0;
   tmp[0] = tmpa[counter];
   tmp[1] = tmpa[counter + 1];
   counter += 2;
   while (i2cWrite(0x06, tmp, 2, false)); // XAxis offset, bytes
   tmp[0] = tmpa[counter];
   tmp[1] = tmpa[counter + 1];
   counter += 2;
   while (i2cWrite(0x08, tmp, 2, false)); // YAxis offset, bytes
   tmp[0] = tmpa[counter];
   tmp[1] = tmpa[counter + 1];
   counter += 2;
   while (i2cWrite(0x0A, tmp, 2, false)); // ZAxis offset, bytes
   tmp[0] = tmpa[counter];
   tmp[1] = tmpa[counter + 1];
   counter += 2;
   while (i2cWrite(0x13, tmp, 2, false)); // XGyro offset, bytes
   tmp[0] = tmpa[counter];
   tmp[1] = tmpa[counter + 1];
   counter += 2;
   while (i2cWrite(0x15, tmp, 2, false)); // YGyro offset, bytes
   tmp[0] = tmpa[counter];
   tmp[1] = tmpa[counter + 1];
   counter += 2;
   while (i2cWrite(0x17, tmp, 2, true)); // ZGyro offset, bytes*/
}

void get_average() {
  long i = 0;
  long buff_accX = 0, buff_accY = 0, buff_accZ = 0; //These are for summing up the readings for averaging
  long buff_gyroX = 0, buff_gyroY = 0, buff_gyroZ = 0; //These are for summing up the readings for averaging
  long buff_magX = 0, buff_magY = 0, buff_magZ = 0; //These are for summing up the readings for averaging
  int16_t tmpaX, tmpaY, tmpaZ, tmpgX, tmpgY, tmpgZ, tmpmX, tmpmY, tmpmZ;
  while (i < (cal_buffer + 76)) { //Discard the first 75 measurememnts
    // read sensors
    while (i2cRead(0x3B, i2cData, 14));
    tmpaX = ((i2cData[0] << 8) | i2cData[1]);
    tmpaY = ((i2cData[2] << 8) | i2cData[3]);
    tmpaZ = ((i2cData[4] << 8) | i2cData[5]);
    tempRaw = (i2cData[6] << 8) | i2cData[7];
    tmpgX = (i2cData[8] << 8) | i2cData[9];
    tmpgY = (i2cData[10] << 8) | i2cData[11];
    tmpgZ = (i2cData[12] << 8) | i2cData[13];

    unsigned char ST1;
    do
    {
      I2Cread(MAG_ADDRESS, 0x02, 1, &ST1);
    }
    while (!(ST1 & 0x01));
    // Read magnetometer data
    uint8_t Mag[7];
    I2Cread(MAG_ADDRESS, 0x03, 7, Mag);
    tmpmX = -(Mag[1] << 8 | Mag[0]);
    tmpmY = -(Mag[3] << 8 | Mag[2]);
    tmpmZ = -(Mag[5] << 8 | Mag[4]);


    if (i > 75 && i <= (cal_buffer + 75)) { //Discard the first 75 measurememnts
      buff_accX = buff_accX + tmpaX;
      buff_accY = buff_accY + tmpaY;
      buff_accZ = buff_accZ + tmpaZ;
      buff_gyroX = buff_gyroX + tmpgX;
      buff_gyroY = buff_gyroY + tmpgY;
      buff_gyroZ = buff_gyroZ + tmpgZ;
      buff_magX = buff_magX + tmpmX;
      buff_magY = buff_magY + tmpmY;
      buff_magZ = buff_magZ + tmpmZ;
    }

    if (i == (cal_buffer + 75)) { //Record the average, when done calibrating
      mean_accX = buff_accX / cal_buffer;
      mean_accY = buff_accY / cal_buffer;
      mean_accZ = buff_accZ / cal_buffer;
      mean_gyroX = buff_gyroX / cal_buffer;
      mean_gyroY = buff_gyroY / cal_buffer;
      mean_gyroZ = buff_gyroZ / cal_buffer;
      mean_magX = buff_magX / cal_buffer;
      mean_magY = buff_magY / cal_buffer;
      mean_magZ = buff_magZ / cal_buffer;
    }
    i++;
    delay(3);
  }
}

void calibrate2()
{
  //Get the average values and record them as point1
  reset_offsets();

  /*get_average();
   //Send a message Calibration done 1
   accX_offset = -mean_accX;
   gyroX_offset = -mean_gyroX;
   gyroY_offset = -mean_gyroY;
   gyroZ_offset = -mean_gyroZ;
   y_val_av1 = mean_accY;
   z_val_av1 = mean_accZ;*/

  Serial.println("Calibration done 1");//Indicate collection at point 1 is done

  while (1) {
    if (Serial.parseInt() == 1) break; //Wait for '1' being sent from the software side
  }

  get_average();//Record the average at another point
  y_val_av2 = mean_accY;
  z_val_av2 = mean_accZ;
  //Fit a line through the two points, get the slope and offset values
  int slopeY = 1000;
  int slopeZ = 1000;
  int offsetY = 0;
  int offsetZ = 0;
  slopeY = int(1000.0 * float((cal_point2[1] - cal_point1[1])) / float((y_val_av2 - y_val_av1))); //slopes be multiplied by a thousand
  slopeZ = int(1000.0 * float((cal_point2[2] - cal_point1[2])) / float((z_val_av2 - z_val_av1))); //slopes be multiplied by a thousand
  offsetY = cal_point2[1] - float(slopeY / 1000.0) * float(y_val_av2);
  offsetZ = cal_point2[1] - float(slopeZ / 1000.0) * float(z_val_av1);

  accX_offset = cal_point1[0] + mean_accX;
  accY_offset = cal_point1[1] + mean_accY;
  accZ_offset = cal_point1[2] + mean_accZ;
  gyroX_offset = mean_gyroX;
  gyroY_offset = mean_gyroY;
  gyroZ_offset = mean_gyroZ;

  //Serial.println(slopeY);
  //Serial.println(slopeZ);
  //Serial.println(offsetY);
  //Serial.println(offsetZ);
  //Record the offsets to the eeprom, then record the slopes
  calibration[0] = highByte(accX_offset); 
  calibration[1] = lowByte(accX_offset);
  calibration[2] = highByte(accY_offset); 
  calibration[3] = lowByte(accY_offset);
  calibration[4] = highByte(accZ_offset); 
  calibration[5] = lowByte(accZ_offset);
  calibration[6] = highByte(gyroX_offset); 
  calibration[7] = lowByte(gyroX_offset);
  calibration[8] = highByte(gyroY_offset); 
  calibration[9] = lowByte(gyroY_offset);
  calibration[10] = highByte(gyroZ_offset); 
  calibration[11] = lowByte(gyroZ_offset);

  //Serial.println("flag1");

  Serial.println("Calibration done 1a");//Indicate collection at point 1 is done

  ///Magnetometer calibration part
  /*----------------------------------------------------------------------------*/
  for(int y=0; y<20; y++)
  read_mag();

  int16_t mag_max[3] = {
    magX, magY, magZ  }
  , mag_min[3] = {
    magX, magY, magZ  },
    mag_scale[3]={0, 0, 0};

  while (1)
  {
    read_mag();
    if (magX > mag_max[0]) mag_max[0] = magX;
    if (magX < mag_min[0]) mag_min[0] = magX;
    if (magY > mag_max[1]) mag_max[1] = magY;
    if (magY < mag_min[1]) mag_min[1] = magY;
    if (magZ > mag_max[2]) mag_max[2] = magZ;
    if (magZ < mag_min[2]) mag_min[2] = magZ;
    printData();
    /*magX_offset  = -(mag_max[0] + mag_min[0]) / 2; // get average x mag bias in counts
    magY_offset  = -(mag_max[1] + mag_min[1]) / 2; // get average y mag bias in counts
    magZ_offset  = -(mag_max[2] + mag_min[2]) / 2; // get average z mag bias in counts
    Serial.print(magX_offset); 
    Serial.print(";");
    Serial.print(magY_offset); 
    Serial.print(";");
    Serial.print(magZ_offset); 
    Serial.println(";");*/
    if (Serial.available())
      if (Serial.parseInt() == 1)
        break;
    delay(40);  // at 8 Hz ODR, new mag data is available every 125 ms
  }

  // Get hard iron correction
  magX_offset  = -(mag_max[0] + mag_min[0]) / 2; // get average x mag bias in counts
  magY_offset  = -(mag_max[1] + mag_min[1]) / 2; // get average y mag bias in counts
  magZ_offset  = -(mag_max[2] + mag_min[2]) / 2; // get average z mag bias in counts
  
  // Get soft iron correction estimate
 mag_scale[0]  = (mag_max[0] - mag_min[0])/2;  // get average x axis max chord length in counts
 mag_scale[1]  = (mag_max[1] - mag_min[1])/2;  // get average y axis max chord length in counts
 mag_scale[2]  = (mag_max[2] - mag_min[2])/2;  // get average z axis max chord length in counts

 float avg_rad = mag_scale[0] + mag_scale[1] + mag_scale[2];
 avg_rad /= 3.0;

 uint16_t Xbias = float(10000)*avg_rad/((float)mag_scale[0]);
 uint16_t Ybias = float(10000)*avg_rad/((float)mag_scale[1]);
 uint16_t Zbias = float(10000)*avg_rad/((float)mag_scale[2]);

  /*----------------------------------------------------------------------------*/
  /*calibration[24] = highByte(magX_offset); calibration[25] = lowByte(magX_offset); calibration[26] = highByte(1000); calibration[27] = lowByte(1000);
   calibration[28] = highByte(magY_offset); calibration[29] = lowByte(magY_offset); calibration[30] = highByte(1000); calibration[31] = lowByte(1000);
   calibration[32] = highByte(magZ_offset); calibration[33] = lowByte(magZ_offset); calibration[34] = highByte(1000); calibration[35] = lowByte(1000);*/
  calibration[12] = uint16_t(magX_offset)/256; 
  calibration[13] = uint16_t(magX_offset)%256;
  calibration[14] = uint16_t(magY_offset)/256; 
  calibration[15] = uint16_t(magY_offset)%256;
  calibration[16] = uint16_t(magZ_offset)/256; 
  calibration[17] = uint16_t(magZ_offset)%256;
  
  calibration[18] = Xbias/256; 
  calibration[19] = Xbias%256;
  calibration[20] = Ybias/256; 
  calibration[21] = Ybias%256;
  calibration[22] = Zbias/256; 
  calibration[23] = Zbias%256;

  //read_calibration();
  
  write_calibration(calibration, 24);
  /*Serial.print(calibration[12]*256+calibration[13]); Serial.print(";");
  Serial.print(calibration[14]*256+calibration[15]); Serial.print(";");
  Serial.print(calibration[16]*256+calibration[17]); Serial.println(";");*/

  /*unsigned char tmpa[18];//for debugging purposes
  for (unsigned int k = 0; k < 18; k++)
    tmpa[k] = EEPROM.read(k);
    
  for (unsigned char i = 0; i < 9; i++)
    {Serial.print(tmpa[2 * i] * 256 + tmpa[2 * i + 1]);Serial.print(";");}
    Serial.println(";");*/
    
  Serial.println("Calibration done 2");//Indicate calibration is done
}
void read_mag()
{
    unsigned char ST1;
  uint8_t Mag[7];
  do
  {
    I2Cread(MAG_ADDRESS, 0x02, 1, &ST1);
  }
  while (!(ST1 & 0x01));
  // Read magnetometer data
  I2Cread(MAG_ADDRESS, 0x03, 7, Mag);
  magX = -(Mag[1] << 8 | Mag[0]);
  magY = -(Mag[3] << 8 | Mag[2]);
  magZ = -(Mag[5] << 8 | Mag[4]);
}

void apply_calibration()
{
  /*accX = calibration_values[0] + int(float(accX) * float(calibration_values[1]) / 1000.0);
   accY = calibration_values[2] + int(float(accY) * float(calibration_values[3]) / 1000.0);
   accZ = calibration_values[4] + int(float(accZ) * float(calibration_values[5]) / 1000.0);
   gyroX = calibration_values[6] + int(float(gyroX) * float(calibration_values[7]) / 1000.0);
   gyroY = calibration_values[8] + int(float(gyroY) * float(calibration_values[9]) / 1000.0);
   gyroZ = calibration_values[10] + int(float(gyroZ) * float(calibration_values[11]) / 1000.0);
   magX = calibration_values[12] + int(float(magX) * float(calibration_values[13]) / 1000.0);
   magY = calibration_values[14] + int(float(magY) * float(calibration_values[15]) / 1000.0);
   magZ = calibration_values[16] + int(float(magZ) * float(calibration_values[17]) / 1000.0);*/
  accX = -calibration_values[0] + accX;
  accY = -calibration_values[1] + accY;
  accZ = calibration_values[2] - accZ;
  gyroX = -calibration_values[3] + gyroX;
  gyroY = -calibration_values[4] + gyroY;
  gyroZ = -calibration_values[5] + gyroZ;
  magX = calibration_values[6] + int(float(magX)*float(calibration_values[9])/float(10000));
  magY = calibration_values[7] + int(float(magY)*float(calibration_values[10])/float(10000));
  magZ = calibration_values[8] + int(float(magZ)*float(calibration_values[11])/float(10000));
}

