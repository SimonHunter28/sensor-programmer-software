#ifndef DIALOG_MAGNETOMETER_H
#define DIALOG_MAGNETOMETER_H

#include <QDialog>
#include <QVector>

namespace Ui {
class Dialog_magnetometer;
}

class Dialog_magnetometer : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_magnetometer(QWidget *parent = 0);
    ~Dialog_magnetometer();
    QVector<double> X, Y, Z;
    double maxX, maxY, maxZ, minX, minY, minZ;
    void update_plots(int, int, int);

private:
    Ui::Dialog_magnetometer *ui;

};

#endif // DIALOG_MAGNETOMETER_H
