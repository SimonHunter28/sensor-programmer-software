#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(check_timeout()));
    timer->start(300);

    hingePort="COM7";
    progPort="COM8";
    btldPort="COM12";
    database_name="database.csv";
    btld_name="OTA_bootloader.hex";
    testFW="Test.hex";
    mainFW="HINGE_A7.hex";

    found_prog=false;
    found_hinge=false;
    found_btld=false;

    flag1=false;
    flag2=false;
    flag3=false;
    flag4=false;
    flag5=false;
    connect_hinge=false;
    connect_btld=false;

    serialPortBaudRate_prog = BAUD19200;
    serialPortBaudRate_hinge =BAUD115200;

    //current_path=QDir::currentPath()+"/release";//Get the path of the executable
    current_path=QDir::currentPath();//Get the path of the executable
    qDebug(current_path.toStdString().c_str());
    firmware_path=current_path+"/Firmware/";
    database_path=current_path+"/Databases/";

    read_defaults();
    ui->textEdit->append("This is the debug log for Hinge programmer");

    connect(&sPort_hinge, SIGNAL(data_available(QString, double*, int)), this, SLOT(add_hinge_data(QString, double*, int)));
    connect(&programmer, SIGNAL(update_ui(QString)), this, SLOT(update_ui(QString)));
    connect(&programmer, SIGNAL(all_done()), this, SLOT(all_done()));
    connect(&programmer, SIGNAL(step1_done()), this, SLOT(prog_done1()));
    connect(&programmer, SIGNAL(step2_done()), this, SLOT(prog_done2()));
    //connect(&programmer, SIGNAL(step4_done()), this, SLOT(prog_done4()));
    connect(this, SIGNAL(prog_trig()), &programmer, SLOT(sequence_trig()));
    connect(&avrdude, SIGNAL(readyReadStandardError()), this, SLOT(readyReadStandardError()));
    connect(&avrdude, SIGNAL(readyReadStandardOutput()), this, SLOT(readyReadStandardOutput()));
    //connect(&sPort_hinge, SIGNAL(cal_done2()), &programmer, SLOT(cal_done()));
    connect(&sPort_hinge, SIGNAL(cal_done2()), this, SLOT(prog_done4()));
    connect(&sPort_hinge, SIGNAL(cal_done3()), this, SLOT(prog_done3()));
    connect(&sPort_hinge, SIGNAL(cal_done1()), this, SLOT(cal_done1()));
    connect(&sPort_hinge, SIGNAL(cal_done1a()), this, SLOT(cal_done1a()));
    connect(&dialog_window, SIGNAL(accepted()), this, SLOT(cal_done1aa()));
    connect(&debugging_window, SIGNAL(accepted()), this, SLOT(finish_yes()));
    connect(&debugging_window, SIGNAL(rejected()), this, SLOT(finish_no()));
    //connect(&dialog_window, SIGNAL(accepted()), this, SLOT(restartt));
    //connect(&dialog_magnetometer_window, SIGNAL(accepted()), this, SLOT(restartt));
    //connect(&debugging_window, SIGNAL(rejected()), this, SLOT(finish()));
    connect(&dialog_magnetometer_window, SIGNAL(accepted()), this, SLOT(cal_done2a()));
}

MainWindow::~MainWindow()
{
    //timer->stop();
    delete ui;
}

void MainWindow::prog_done1()
{
    if(flag1)
    {
        programmer.trigger_flag1=false;
        ui->checkBox->setChecked(true);

        connect_btld=true;
        check_ports();
    }
    //qDebug(QString(ui->textEdit->toPlainText()).toStdString().c_str());
}
void MainWindow::prog_done2()
{
    if(flag2)
    {
        programmer.trigger_flag2=false;
        ui->checkBox_2->setChecked(true);
        connect_hinge=true;
        //programmer.trigger_flag3=true;
        check_ports();
    }
}
void MainWindow::prog_done3()
{
    if(flag3)
    {
        programmer.trigger_flag3=false;
        ui->checkBox_3->setChecked(true);
        programmer.program_sequence4();
        check_ports();
        //connect_hinge=true;
        //programmer.trigger_flag3=true;
    }
}
void MainWindow::prog_done4()
{
    flag4=true;
    connect_btld=true;
}

void MainWindow::check_timeout()
{
    check_ports();

    if(connect_hinge&found_hinge&!sPort_hinge.connected)
    {
        sPort_hinge.conn(QString("%1").arg(hingePort), serialPortBaudRate_hinge);
    }
    if(sPort_hinge.connected)
    {
        if(flag2&!flag3)
        {
            qDebug("flag1");
            programmer.trigger_flag3=true;
            connect_hinge=false;
        }
    }

    if(connect_btld&found_hinge&!found_btld)
    {
        qDebug("flag2");
        if(sPort_hinge.connected)
            sPort_hinge.disconn();
        sPort_hinge.conn(QString("%1").arg(hingePort), BAUD1200);
        sleeper.msleep(300);
        sPort_hinge.disconn();
    }
    if(found_btld&connect_btld)
    {
        if(flag1&!flag2)
        {
            qDebug("flag3");
            programmer.trigger_flag2=true;
            connect_btld=false;
        }
        if(flag2&flag3&flag4&!flag5)
        {
            qDebug("flag4");
            programmer.trigger_flag4=true;
            connect_btld=false;
        }
    }
    programmer.program_sequence();
}

void MainWindow::check_ports()
{
    scanner.scan(&found_prog,&found_hinge,&found_btld, &progPort, &hingePort, &btldPort);

    if(found_prog)
    {
        ui->lineEdit_11->setText(progPort);
        if(sPort_prog.connected)
            ui->lineEdit_11->setStyleSheet("QLineEdit{color: green;}");
        else
            ui->lineEdit_11->setStyleSheet("QLineEdit{color: black;}");

    }
    else
        ui->lineEdit_11->setText("");

    if(found_hinge)
    {
        ui->lineEdit_12->setText(hingePort);
        if(sPort_hinge.connected)
            ui->lineEdit_12->setStyleSheet("QLineEdit{color: green;}");
        else
            ui->lineEdit_12->setStyleSheet("QLineEdit{color: black;}");

    }
    else
        ui->lineEdit_12->setText("");

    if(found_btld)
        ui->lineEdit_13->setText(btldPort);
    else
        ui->lineEdit_13->setText("");

    programmer.found_btld=found_btld;
    programmer.found_hinge=found_hinge;
    programmer.found_prog=found_prog;

}

void MainWindow::cal_done1()
{
    dialog_window.show();
    //sPort_hinge.send_line("1");
}

void MainWindow::cal_done1aa()
{
    ui->checkBox_4->setChecked(true);
    dialog_window.hide();
    sPort_hinge.send_line("1");
}

void MainWindow::cal_done1a()
{
    //ui->lineEdit_7->setText("Ready to calibrate magnetometer");
    dialog_magnetometer_window.show();
    dialog_magnetometer_window.X.clear();
    dialog_magnetometer_window.Y.clear();
    dialog_magnetometer_window.Z.clear();
    dialog_magnetometer_window.minX=65535;
    dialog_magnetometer_window.maxX=-65535;
    dialog_magnetometer_window.minY=65535;
    dialog_magnetometer_window.maxY=-65535;
    dialog_magnetometer_window.minZ=65535;
    dialog_magnetometer_window.maxZ=-65535;
}

void MainWindow::cal_done2a()
{
    dialog_magnetometer_window.hide();
    ui->checkBox_5->setChecked(true);
    sPort_hinge.send_line("1");
}

void MainWindow::all_done()
{
    programmer.trigger_flag4=false;
    flag5=true;
    sleeper.msleep(100);

    while(!sPort_hinge.connected)
    {
        sPort_hinge.conn(QString("%1").arg(hingePort), serialPortBaudRate_hinge);
        sleeper.msleep(200);
    }

    sPort_hinge.send_line("2\n");

    ui->checkBox_6->setChecked(true);
    //timer->start(300);
}

void MainWindow::update_ui(QString tmp)
{
    QString match1 = QString("avrdude: 1 bytes of lfuse verified");
    QString match2 = QString("18608 bytes of flash written");
    QString match3 = QString("Setting BT name");
    QString match4 = QString("28220 bytes of flash written");
    //qDebug(tmp.toStdString().c_str());
    if(tmp.contains(match1))
        flag1=true;
    if(tmp.contains(match2))
        flag2=true;
    if(tmp.contains(match3))
        flag3=true;
    if(tmp.contains(match4))
        flag5=true;

    ui->textEdit->append(tmp);
}

void MainWindow::finish_yes()
{
    if(sPort_hinge.connected)
        sPort_hinge.disconn();
}

void MainWindow::finish_no()
{
    if(sPort_hinge.connected)
        sPort_hinge.disconn();
}

void MainWindow::add_hinge_data(QString raw, double *input, int length)
{
    char tmp[128];
    if(length==9)
    {

        sprintf(tmp, "IMU values: %d %d %d %d %d %d\n", int(input[0]), int(input[1]), int(input[2]), int(input[3]), int(input[4]), int(input[5]), int(input[6]), int(input[7]), int(input[8]));
        //ui->textEdit->append(tmp);
        dialog_magnetometer_window.update_plots(int(input[6]), int(input[7]), int(input[8]));
    }
    else
    {
        if(length==10)
        {

            ui->lineEdit_10->setText(QString::number((int(input[4])+int(input[5])+int((16384+input[6]))+int(input[7])+int(input[8])+int(input[9]))));
            ui->lineEdit_9->setText(QString::number(input[2]));
        }
        else
            if(length==14)
            {
                if(debugging_window.isHidden())
                {
                    debugging_window.show();
                    debugging_window.X.clear();
                    debugging_window.Y.clear();
                    debugging_window.Z.clear();
                    debugging_window.minX=65535;
                    debugging_window.maxX=-65535;
                    debugging_window.minY=65535;
                    debugging_window.maxY=-65535;
                    debugging_window.minZ=65535;
                    debugging_window.maxZ=-65535;
                }
                else
                    debugging_window.update_plots(int(input[11]), int(input[12]), int(input[13]),
                            int(input[4]), int(input[2]), int(input[3]));
            }
            else
                qDebug(raw.toStdString().c_str());
    }
}

void MainWindow::read_defaults()
{
    /*------------------------------------------------------Import default port names, etc.---------------------------*/
    QString tmp=database_path+"/defaults.csv";
    QFile data(tmp);
    QStringList list;
    if(data.open(QFile::ReadOnly|QFile::Truncate))
    {
        QTextStream input(&data);

        while(!input.atEnd())
        {
            QString line_tmp=input.readLine().toStdString().c_str();
            QRegExp rx("([^,]*[,\n])");//This regexp should segment comma separators
            unsigned int pos=0;
            while ((pos = rx.indexIn(line_tmp, pos)) != -1)//Separates the string into words
            {
                QString t=rx.cap(1);
                t.remove(t.length()-1, 1);//Remove the last separator character
                list << t;
                pos += rx.matchedLength();
            }
        }
        hingePort=list.at(0);
        progPort=list.at(2);
        btldPort=list.at(1);
        database_name=list.at(6);
        btld_name=list.at(3);
        testFW=list.at(4);
        mainFW=list.at(5);
        data.close();

        ui->lineEdit_11->setText(hingePort);
        ui->lineEdit_12->setText(progPort);
        ui->lineEdit_13->setText(btldPort);
    }
    else
        ui->textEdit->append("Cannot find defaults file");
    /*------------------------------------------------------Import default port names, etc.---------------------------*/

}

void MainWindow::on_pushButton_clicked()
{
    if(!flag1)
    {
        programmer.firmware_path=firmware_path;
        programmer.sPort_prog=&sPort_prog;
        programmer.sPort_hinge=&sPort_hinge;
        programmer.btld_name=btld_name;
        programmer.hingePort=hingePort;
        programmer.btldPort=btldPort;
        programmer.progPort=progPort;
        programmer.serialPortBaudRate_hinge=serialPortBaudRate_hinge;
        programmer.serialPortBaudRate_prog=serialPortBaudRate_prog;
        programmer.database_path=database_path;
        programmer.database_name=database_name;
        programmer.mainFW=mainFW;
        programmer.testFW=testFW;

        //ui->lineEdit_4->setStyleSheet("QLineEdit{color: gray;}");
        //programmer.sequence_trig();
        //timer->stop();
        emit prog_trig();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->textEdit->append("Resetting");

    if(sPort_hinge.connected)
        sPort_hinge.disconn();

    if(sPort_prog.connected)
    {
        sPort_prog.send_line("d\n");//Exit into the programmer mode
        sPort_prog.disconn();
    }

    dialog_magnetometer_window.X.clear();
    dialog_magnetometer_window.Y.clear();
    dialog_magnetometer_window.Z.clear();
    dialog_magnetometer_window.minX=65535;
    dialog_magnetometer_window.maxX=-65535;
    dialog_magnetometer_window.minY=65535;
    dialog_magnetometer_window.maxY=-65535;
    dialog_magnetometer_window.minZ=65535;
    dialog_magnetometer_window.maxZ=-65535;

    flag1=false;
    flag2=false;
    flag3=false;
    flag4=false;
    flag5=false;
    connect_hinge=false;
    connect_btld=false;
    ui->checkBox->setChecked(false);
    ui->checkBox_2->setChecked(false);
    ui->checkBox_3->setChecked(false);
    ui->checkBox_4->setChecked(false);
    ui->checkBox_5->setChecked(false);
    ui->checkBox_6->setChecked(false);
}

void MainWindow::readyReadStandardOutput(){
    //qDebug(avrdude.readAllStandardOutput());
    QString tmp=avrdude.readAllStandardOutput();
    ui->textEdit->append(tmp);

}

void MainWindow::readyReadStandardError(){
    //qDebug(avrdude.readAllStandardError().toStdString().c_str());
    ui->textEdit->append(avrdude.readAllStandardError());


}
