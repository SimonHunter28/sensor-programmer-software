#include "debugging.h"
#include "ui_debugging.h"

debugging::debugging(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::debugging)
{
    ui->setupUi(this);
}

debugging::~debugging()
{
    delete ui;
}

void debugging::update_plots(int X_tmp, int Y_tmp, int Z_tmp, int Yaw, int Pitch, int Roll)
{
    if(X_tmp>maxX) maxX=X_tmp;
    if(X_tmp<minX) minX=X_tmp;
    if(Y_tmp>maxY) maxY=Y_tmp;
    if(Y_tmp<minY) minY=Y_tmp;
    if(Z_tmp>maxZ) maxZ=Z_tmp;
    if(Z_tmp<minZ) minZ=Z_tmp;
    ui->label->setText(QString::number(maxX-minX));
    ui->label_2->setText(QString::number(maxY-minY));
    ui->label_3->setText(QString::number(maxZ-minZ));
    ui->label_4->setText(QString::number((maxX+minX)/2));
    ui->label_5->setText(QString::number((maxY+minY)/2));
    ui->label_6->setText(QString::number((maxZ+minZ)/2));
    ui->label_7->setText(QString::number(Yaw));
    ui->label_8->setText(QString::number(Pitch));
    ui->label_9->setText(QString::number(Roll));

    X.append(X_tmp);
    Y.append(Y_tmp);
    Z.append(Z_tmp);

    ui->graphicsView->clearItems();
    ui->graphicsView->clearGraphs();
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(0)->setData(X, Y);
    //ui->graphicsView->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 10))); // first graph will be blue
    //ui->graphicsView->graph(0)->setPen(QPen(QColor(0, 0, 255, 100))); // first graph will be blue
    //ui->graphicsView->graph(0)->setPen(QPen(Qt::blue)); // first graph will be blue
    ui->graphicsView->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->graphicsView->addGraph();
    ui->graphicsView->graph(1)->setData(X, Z);
    //ui->graphicsView->graph(1)->setBrush(QBrush(QColor(255, 0, 0, 10))); // second graph will be red
    ui->graphicsView->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui->graphicsView->graph(1)->setScatterStyle(QCPScatterStyle::ssDisc);
    //ui->graphicsView->graph(1)->setPen(QPen(QColor(255, 0, 0, 100))); // first graph will be blue
    ui->graphicsView->graph(1)->setPen(QPen(Qt::red)); // first graph will be blue
    ui->graphicsView->yAxis->setLabel("Y, Z values");
    ui->graphicsView->xAxis->setLabel("X values");
    // set axes ranges, so we see all data:
    ui->graphicsView->xAxis->setRange(minX, maxX);
    int tmp_min=minY, tmp_max=maxY;
    if(minY>minZ)
    tmp_min=minZ;
    if(maxY<maxZ)
    tmp_max=maxZ;
    ui->graphicsView->yAxis->setRange(tmp_min, tmp_max);
    //ui->graphicsView->rescaleAxes();
    ui->graphicsView->replot();
}
