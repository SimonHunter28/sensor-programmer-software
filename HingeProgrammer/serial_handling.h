#ifndef SERIAL_HANDLING_H
#define SERIAL_HANDLING_H
#include <QThread>
#include "QtExtSerialPort/qextserialport.h"
#include <QList>
#include <QString>
#include <QTime>

class serial_handling: public QThread
{
    Q_OBJECT
public:
    serial_handling();
    ~serial_handling();

    char *buf;
    bool connected;
    double *val;
    double tempr;
    void send_line(const char*);
    void conn(QString, BaudRateType);
    void disconn();

    //simulator sim;
    QTime time1;

protected:
    void run();

private:
    int buf_len;
    QextSerialPort *port;

signals:
    void data_available(QString, double *, int);
    void cal_done1();
    void cal_done1a();
    void cal_done2();
    void cal_done3();

private slots:
    void get_line();
    //void mockup(QString);
};


#endif // SERIAL_HANDLING

