#ifndef PROG_H
#define PROG_H

#include <QObject>
#include <QThread>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <serial_handling.h>
#include <sleeper.h>
#include <QProcess>//This is for AvrDude.exe calling

class prog : public QThread
{
    Q_OBJECT
public:
    prog(QObject *parent = 0);
    ~prog();

    serial_handling *sPort_prog;
    serial_handling *sPort_hinge;
    QProcess avrdude;
    QString firmware_path;
    QString mainFW;
    QString testFW;
    Sleeper sleeper;
    QString database_path;
    QString database_name;
    QString program;
    QStringList arguments;
    bool found_prog, found_hinge, found_btld;
    BaudRateType serialPortBaudRate_hinge;
    BaudRateType serialPortBaudRate_prog;

    QString progPort;
    QString hingePort;
    QString btldPort;

    bool trigger_flag1;
    bool trigger_flag2;
    bool trigger_flag3;
    bool trigger_flag4;

    QString btld_name;
    int hinge_nr;
    bool hinge_type;

    void program_sequence();
    void program_sequence1();
    void program_sequence2();
    void program_sequence3();
    void program_sequence4();


protected:
    void run();
private slots:
    void update1();
    void update2();
public slots:
    void sequence_trig();
    void cal_done();
signals:
    void update_ui(QString);
    void all_done();
    void step1_done();
    void step2_done();
    void step3_done();
    void step4_done();
    void step5_done();
};

#endif // PROG_H
