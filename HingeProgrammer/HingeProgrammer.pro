#-------------------------------------------------
#
# Project created by QtCreator 2016-06-10T18:08:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = HingeProgrammer4
TEMPLATE = app
QT += serialport
CONFIG += extserialport

SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    dialog_magnetometer.cpp \
    debugging.cpp \
    qcustomplot.cpp \
    serial_connect.cpp \
    serial_handling.cpp \
    sleeper.cpp \
    prog.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    dialog_magnetometer.h \
    debugging.h \
    qcustomplot.h \
    serial_connect.h \
    serial_handling.h \
    sleeper.h \
    prog.h

FORMS    += mainwindow.ui \
    dialog.ui \
    dialog_magnetometer.ui \
    debugging.ui
