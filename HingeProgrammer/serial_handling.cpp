#include "serial_handling.h"

#include <QRegExp>
#include <QString>
#include <QStringList>
#include <QTime>

serial_handling::serial_handling()
{
    buf_len=1024;//Buffer length for reading the incoming string
    buf=new char[buf_len];//Buffer for reading the incoming string
    val= new double[buf_len];//Buffer for storing immediate channel voltage values
    connected=false;
}

serial_handling::~serial_handling()
{
    if(port->isOpen())
    {
        port->close();//closes the port upon class destruction, avoids rogue handles
        connected=false;
    }
}

void serial_handling::disconn()//disconnect arduino
{
    if(port->isOpen())
    {
        port->close();
        connected=false;
    //else
        //disconnect(&sim, SIGNAL(cuckoo(QString)), this, SLOT(mockup(QString)));
    qDebug("Port disconnected");
    }
}

void serial_handling::conn(QString address, BaudRateType baud)//This sets up the serial port for communications and connects the simulator signal
{
    port = new QextSerialPort(address.toStdString().c_str()); //we create the port
    if(port->open(QIODevice::ReadWrite | QIODevice::Unbuffered)) //we open the port)
    {
        connected=true;
        //Set the port properties
        port->setBaudRate(baud);
        port->setFlowControl(FLOW_OFF);
        port->setParity(PAR_NONE);
        port->setDataBits(DATA_8);
        port->setStopBits(STOP_1);

        connect(port, SIGNAL(readyRead()), this, SLOT(get_line()));
        port->flush();//Get rid of stuff on the buffer already
        qDebug("Connected port: %s", address.toStdString().c_str());
    }
    else
    {
        qDebug("Connecting failed");
        //connect(&sim, SIGNAL(cuckoo(QString)), this, SLOT(mockup(QString)));
    }

}

/*void serial_handling::mockup(QString incoming)//This function receives data from a simulation script for offline data processing
{
    QStringList list;//This variable stores the values in a QString format as a list

    if(incoming.length()>5)//wait until a whole line has arrived
    {
        //qDebug("bytesAvailable: %d", port->bytesAvailable());//Shows how many bytes have arrived

        QRegExp rx("([-\\d.]+)");//This regexp should detect rational numbers

        QString str = incoming;//Converts char* to QString
        //qDebug("Header: %s", str.toStdString().c_str());
        unsigned int pos=0;

        while ((pos = rx.indexIn(str, pos)) != -1) {//Separates the string into numbers
            list << rx.cap(1);
            pos += rx.matchedLength();
        }
        //qDebug("Read values: %d", list.length());//For debugging purposes, this shows how many values have been read in

        //qDebug("Element %d: %s", 1, list.at(1).toStdString().c_str());

        if((str.at(0)=='A')&(list.length()==19))
        {
            double output[19];
            for(unsigned int i=0; i<19; i++)
                output[i]=list.at(i).toDouble();
            emit data_available(output);
        }
    }
}*/

void serial_handling::send_line(const char *str)//This sends a value followed by a character
{
    if(port->isOpen())
    {
        port->write(str); //send the buffer
        qDebug("Sent: %s",str); //For debugging purposes outputs what is being sent out
    }
}

void serial_handling::get_line()//This script receives data from arduino and formats into numeric values
{
    QStringList list;//This variable stores the values in a QString format as a list

    if(port->canReadLine())//wait until a whole line has arrived
    {
        //qDebug("bytesAvailable: %d", port->bytesAvailable());//Shows how many bytes have arrived
        port->readLine(buf, buf_len);//Reads the line, which ends with a newline character
        QString temp = QString(buf);
        QString match1 = QString("Calibration done 1");
        QString matcha1 = QString("Calibration done 1a");
        QString match2 = QString("Calibration done 2");
        QString match3 = QString("SN,Hinge-Sensor");

        if(temp.contains(matcha1))
            emit cal_done1a();
        else if(temp.contains(match1))
            emit cal_done1();
        if(temp.contains(match2))
            emit cal_done2();
        if(temp.contains(match3))
            emit cal_done3();

        QRegExp rx("([-\\d.]+)");//This regexp should detect rational numbers

        QString str = QString::fromStdString(buf);//Converts char* to QString
        //qDebug("Raw string: %s", str.toStdString().c_str());
        unsigned int pos=0;

        while ((pos = rx.indexIn(str, pos)) != -1) {//Separates the string into numbers
            list << rx.cap(1);
            pos += rx.matchedLength();
        }
        //qDebug("Read values: %d", list.length());//For debugging purposes, this shows how many values have been read in
        //qDebug("Element %d: %s", 1, list.at(1).toStdString().c_str());

        double output[list.length()];
        for(unsigned int i=0; i<list.length(); i++)
            output[i]=list.at(i).toDouble();

        emit data_available(str, output, list.length());
    }
}

void serial_handling::run()
{
    //Don't mess with this one, this comes from QThread...
}
