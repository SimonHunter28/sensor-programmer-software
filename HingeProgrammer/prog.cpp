#include "prog.h"

prog::prog(QObject *parent):QThread(parent)
{
    trigger_flag1=false;
    trigger_flag2=false;
    trigger_flag3=false;
    trigger_flag4=false;
    connect(&avrdude, SIGNAL(readyReadStandardError()), this, SLOT(update1()));
    connect(&avrdude, SIGNAL(readyReadStandardOutput()), this, SLOT(update2()));
}

prog::~prog()
{
    wait();
}

void prog::sequence_trig()
{
    program = firmware_path+"avrdude";
    trigger_flag1=true;
}

void prog::program_sequence()
{
    //qDebug("Check2;");
    if(trigger_flag4)
        cal_done();

    if(trigger_flag3)
    {
        program_sequence3();
        trigger_flag3=false;
    }

    if(trigger_flag2)
        program_sequence2();

    if(trigger_flag1)
        program_sequence1();

}

void prog::program_sequence1()
{
    //Start avrdude for bootloader uploading
    qDebug(program.toStdString().c_str());

    emit update_ui(QString("Burning bootloader"));

    arguments=QStringList();
    arguments.append("-C"+firmware_path+"avrdude.conf");
    arguments.append("-v");
    arguments.append("-patmega32u4");
    arguments.append("-carduino");
    arguments.append(QString("-P%1").arg(progPort));
    arguments.append("-b19200");
    arguments.append("-e");
    arguments.append("-Uflash:w:"+firmware_path+btld_name+":i");
    arguments.append("-Ulock:w:0x3F:m");
    arguments.append("-Uefuse:w:0xcb:m");
    arguments.append("-Uhfuse:w:0xd8:m");
    arguments.append("-Ulfuse:w:0xff:m");

    avrdude.start(program, arguments);
    qDebug(avrdude.readAllStandardOutput().toStdString().c_str());
    avrdude.waitForFinished();
    emit step1_done();
}

void prog::program_sequence2()
{

    //Start avrdude for testFW uploading
    /*sPort_hinge->conn(QString("%1").arg(hingePort), 1200);
    sleeper.msleep(300);
    sPort_hinge->disconn();
    sPort_hinge->conn(QString("%1").arg(hingePort), 1200);
    sleeper.msleep(300);
    sPort_hingedisconn();
    sleeper.msleep(500);//I think this is unecessary for a bootlader port*/

    program = firmware_path+"avrdude";
    arguments=QStringList();

    arguments.append("-C"+firmware_path+"avrdude.conf");
    arguments.append("-v");
    arguments.append("-patmega32u4");
    arguments.append("-cavr109");
    arguments.append(QString("-P%1").arg(btldPort));
    arguments.append("-b57600");
    arguments.append("-D");
    arguments.append("-Uflash:w:"+firmware_path+testFW+":i");

    emit update_ui("Uploading test firmware");

    avrdude.start(program, arguments);
    avrdude.waitForFinished();
    //sleeper.msleep(1000);
    emit step2_done();
}

void prog::program_sequence3()
{
    if(sPort_hinge->connected)
    {
        char tmp[128];
        sprintf(tmp, "Hinge-Sensor $\n");
        sPort_hinge->send_line(tmp);
        emit update_ui("Setting BT name");
        emit step3_done();
    }
    else
        emit update_ui("Could not connect Hinge");
}

void prog::program_sequence4()
{
    if(sPort_hinge->connected)
    {
        char tmp[128];
        sprintf(tmp, "+\n");
        sPort_hinge->send_line(tmp);
        emit update_ui("Starting calibration");
    }
    else
        emit update_ui("Couold not connect Hinge");
}

void prog::run()
{
    //Don't mess with this one, this comes from QThread...
}

void prog::cal_done()
{

    /*if(sPort_hinge->connected)
    {
        sPort_hinge->disconn();
    }
    sleeper.msleep(300);

    sPort_hinge->conn(QString("%1").arg(hingePort), BAUD1200);
    sleeper.msleep(300);
    sPort_hinge->disconn();
    sPort_hinge->conn(QString("%1").arg(hingePort), BAUD1200);
    sleeper.msleep(300);
    sPort_hinge->disconn();
    sleeper.msleep(500);//I think this is unecessary for a bootlader port*/

    QString program = firmware_path+"avrdude";
    QStringList arguments;

    arguments.append("-C"+firmware_path+"avrdude.conf");
    arguments.append("-v");
    arguments.append("-patmega32u4");
    arguments.append("-cavr109");
    arguments.append(QString("-P%1").arg(btldPort));
    arguments.append("-b57600");
    arguments.append("-D");
    arguments.append("-Uflash:w:"+firmware_path+mainFW+":i");
    emit update_ui("Uploading full firmware");

    avrdude.start(program, arguments);
    avrdude.waitForFinished();

    /*-------------     Append the database  -----------------------*/
    QString tmp=database_path+database_name;
    QFile data(tmp);
    emit update_ui("Appending database");
    if(data.open(QFile::ReadWrite|QIODevice::Append))
    {
        QTextStream output(&data);
        QDateTime datetime=QDateTime::currentDateTime();
        char temp[128];
        sprintf(temp, "Hinge-Sensor,%d/%d/%d,%d:%d\n\r", datetime.date().day(), datetime.date().month(), datetime.date().year(), datetime.time().hour(), datetime.time().minute());
        output << temp;
        data.close();
    }
    else
        emit update_ui("Cannot find database file");

    emit all_done();
}

void prog::update1()
{
    emit update_ui(avrdude.readAllStandardError());
}
void prog::update2()
{
    QString tmp=avrdude.readAllStandardOutput();
    emit update_ui(tmp);
}
