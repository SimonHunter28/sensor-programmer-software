#ifndef DEBUGGING_H
#define DEBUGGING_H

#include <QDialog>
#include <QVector>

namespace Ui {
class debugging;
}

class debugging : public QDialog
{
    Q_OBJECT

public:
    explicit debugging(QWidget *parent = 0);
    ~debugging();
    QVector<double> X, Y, Z;
    double maxX, maxY, maxZ, minX, minY, minZ;
    void update_plots(int, int, int,int, int, int);

private:
    Ui::debugging *ui;
};

#endif // DEBUGGING_H
