#ifndef SERIAL_CONNECT_H
#define SERIAL_CONNECT_H
#include "QThread"
#include "QSerialPortInfo"

class serial_connect: public QThread
{
    Q_OBJECT
public:
    serial_connect();
    void scan(bool*, bool*, bool*, QString *,QString  *,QString  *);
protected:
    void run();
};

#endif // SERIAL_CONNECT_H
