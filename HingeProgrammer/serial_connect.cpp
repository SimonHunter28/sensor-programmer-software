#include "serial_connect.h"

serial_connect::serial_connect()
{

}

void serial_connect::scan(bool *found_pressure, bool *found_optics, bool *found_btld, QString *pressure_port, QString  *optics_port, QString  *btld_port)
{
    // Example use QSerialPortInfo
    *found_pressure=false;
    *found_optics=false;
    *found_btld=false;

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QString tmp=info.portName();
        if(strcmp(tmp.toStdString().c_str(), pressure_port->toStdString().c_str())==0)
            *found_pressure=true;
        if(strcmp(tmp.toStdString().c_str(), optics_port->toStdString().c_str())==0)
            *found_optics=true;
        if(strcmp(tmp.toStdString().c_str(), btld_port->toStdString().c_str())==0)
            *found_btld=true;

        /*if(strcmp(info.description().toStdString().c_str(), "Arduino Micro")==0)
        {
            *pressure_port=tmp;
            found_pressure=1;
        }
        if(strcmp(info.description().toStdString().c_str(), "Arduino Leonardo")==0)
        {
            *optics_port=tmp;
            found_optics=1;
        }
        if(strcmp(info.description().toStdString().c_str(), "Arduino Leonardo")==0)
        {
            *btld_port=tmp;
            found_optics=1;
        }*/
        /*qDebug(info.portName().toStdString().c_str());
        qDebug(info.description().toStdString().c_str());
        qDebug(info.manufacturer().toStdString().c_str());*/

    }
    /*if(found_optics==0) *optics_port="";
    if(found_pressure==0) *pressure_port="";*/
}

void serial_connect::run()
{

}
