#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTimer>
#include <QDir>
#include "dialog.h"
#include "dialog_magnetometer.h"
#include "debugging.h"
#include "serial_handling.h"
#include "serial_connect.h"
#include "sleeper.h"
#include "prog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    bool found_prog, found_hinge, found_btld;
    QTimer *timer;
    Ui::MainWindow *ui;
    Dialog dialog_window;
    Dialog_magnetometer dialog_magnetometer_window;
    debugging debugging_window;
    serial_connect scanner;
    QString hingePort;
    QString progPort;
    QString btldPort;
    Sleeper sleeper;

    serial_handling sPort_prog;
    serial_handling sPort_hinge;
    prog programmer;

    QString database_name;
    QString btld_name;
    QString testFW;
    QString mainFW;
    QString current_path;
    QString firmware_path;
    QString database_path;

    BaudRateType serialPortBaudRate_hinge;
    BaudRateType serialPortBaudRate_prog;

    bool flag1,flag2,flag3,flag4,flag5;
    bool connect_hinge, connect_btld;


    std::vector<double> min, zero, max, l_time;

    QProcess avrdude;

    void read_defaults();
    void check_ports();

    //void import_database();
public slots:
    void add_hinge_data(QString, double*, int);
    void update_ui(QString);
    void all_done();
    void cal_done2a();
    void cal_done1a();
    void cal_done1aa();
    void cal_done1();
    void readyReadStandardOutput();
    void readyReadStandardError();

    void prog_done1();
    void prog_done2();
    void prog_done3();
    void prog_done4();

    void finish_yes();
    void finish_no();

private slots:
    void check_timeout();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

signals:
    void prog_trig();
};

#endif // MAINWINDOW_H
