/*--------------------- USE Internal EEPROM (1kb) for storing calibration values -------------------------------*/
void read_calibration()
{
  unsigned char tmpa[24];//18 offsets with ints
  for (unsigned int k = 0; k < 24; k++)
    tmpa[k] = EEPROM.read(k);

  for (unsigned int h = 0; h < 12; h++)
  {
    //Serial.println(tmpa[h * 2] * 256 + tmpa[h * 2 + 1]);
    calibration_values[h] = tmpa[2 * h] * 256 + tmpa[2 * h + 1];
  }
    
  //Write the values to the IMU
  /*unsigned char tmp[2];
  int counter = 0;
  tmp[0] = tmpa[counter];
  tmp[1] = tmpa[counter + 1];
  counter += 2;
  while (i2cWrite(0x06, tmp, 2, false)); // XAxis offset, bytes
  tmp[0] = tmpa[counter];
  tmp[1] = tmpa[counter + 1];
  counter += 2;
  while (i2cWrite(0x08, tmp, 2, false)); // YAxis offset, bytes
  tmp[0] = tmpa[counter];
  tmp[1] = tmpa[counter + 1];
  counter += 2;
  while (i2cWrite(0x0A, tmp, 2, false)); // ZAxis offset, bytes
  tmp[0] = tmpa[counter];
  tmp[1] = tmpa[counter + 1];
  counter += 2;
  while (i2cWrite(0x13, tmp, 2, false)); // XGyro offset, bytes
  tmp[0] = tmpa[counter];
  tmp[1] = tmpa[counter + 1];
  counter += 2;
  while (i2cWrite(0x15, tmp, 2, false)); // YGyro offset, bytes
  tmp[0] = tmpa[counter];
  tmp[1] = tmpa[counter + 1];
  counter += 2;
  while (i2cWrite(0x17, tmp, 2, true));*/ // ZGyro offset, bytes
}


/*--------------------- USE External Flash (64Mb) for logging data -------------------------------*/

//Organisation of one page:
//2bytes X - Accel
//2bytes Y - Accel
//2bytes Z - Accel
//2bytes X - Gyro
//2bytes Y - Gyro
//2bytes Z - Gyro
//4bytes T - runtime in milliseconds
/*--------------------------------------------- Total page 16 bytes ------------------------------*/

void dump_16(long address) //d=dump flash area of 16 pages (16 bytes each)
{
  int length=16*16;
  char tmp[length];//read buffer
  char tmps[64];//Serial print buffer
  flash.readBytes(address, tmp, length);
  for(unsigned char k=0; k<length/16; k++)
  {
    sprintf(tmps, "D%d %d %d %d %d %dX %d %d", tmp[16*k]*256+tmp[16*k+1], tmp[16*k+2]*256+tmp[16*k+3], 
    tmp[16*k+4]*256+tmp[16*k+5], tmp[16*k+6]*256+tmp[16*k+7], tmp[16*k+8]*256+tmp[16*k+9], tmp[16*k+10]*256+tmp[16*k+11], tmp[16*k+12]*256+tmp[16*k+13], tmp[16*k+14]*256+tmp[16*k+15]);
    Serial.println(tmps);
    Serial1.println(tmps);
  }
}

void erase_flash()
{   
  Serial.print("Erasing Flash chip: ");
  flash.chipErase();
  while(flash.busy())
  {
    Serial.print(".");
    delay(400);
  }
  Serial.println("DONE"); 
}

void clean_flash()
{
  /* Serial.print("DeviceID: ");
   Serial.println(flash.readDeviceId(), HEX);*/

  for(long k=0; k<64000000; k+=32769)
  {
    Serial.print("Cleaning 32768 block: ");
    Serial.println(k);
    while(flash.busy());
    if((flash.readByte(long(32768)*k)==0xFF)&(flash.readByte(long(32768)*k+long(1))==0xFF)&(flash.readByte(long(32768)*k+long(2))==0xFF))
      break;
    else
      flash.blockErase32K(k);
  }
  current_address=0;
}

void write_page(long address, int *input)
{
  //Serial.print("Writing calibration values ... ");
  char tmp[16];//Bytes to be written from int array
  for(unsigned char g=0; g<8; g++)
  {
    tmp[g*2]=input[g]/256;//Most significant bytes first
    tmp[g*2+1]=input[g]%256;//Least significant bytes first
  }
  while(flash.busy());
  flash.writeBytes(address, tmp, 16);

}

void flash_init()
{
  if (flash.initialize())
    Serial.println("Memory OK!");
  else
    Serial.println("Memory FAIL!");
}

void log_data()
{
  int tmp[]={accX, accY, accZ, gyroX, gyroY, gyroZ, millis()/65536, millis()%65536};
  write_page(current_address, tmp);
  current_address+=long(16);
}








