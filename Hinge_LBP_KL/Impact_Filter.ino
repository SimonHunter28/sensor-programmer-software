const int filterSize = 5;

float f_total, max_array, min_array;
float f_array[filterSize];
int f_index;

void Impact_Filter()
{        
  f_array[f_index] = two_pi_mapping(pitch); 
  max_array = f_array[f_index];
  min_array = f_array[f_index];
  for (int j = 0; j<filterSize; j++)
  {
    if(max_array<f_array[j])
      max_array = f_array[j];

    else if(min_array>f_array[j])
      min_array=f_array[j];
  }
  delta = abs(max_array - min_array); 
 
  f_index ++;   
  
  if (f_index >= filterSize)              
     f_index = 0;      
}



