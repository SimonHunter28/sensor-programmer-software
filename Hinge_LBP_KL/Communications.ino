void check_serial()
{
  int BT_connection = digitalRead(BT_CONNECT);

//  // Prevent BT module reset when BT connected
//  if(BT_connection == 1 && (devicestate == 1 || devicestate == 4) && standby == 1){
////    t_BTreset = millis();
////    Serial.println("No disconnect");
//  }
//  

//  // Execute standby state if BT connection dropped
//  if(BT_connection == 0 && devicestate == 2){
//    devicestate = 1;
//    executeDeviceState(devicestate);
//    }
    
  //Serial.println(digitalRead(BT_CONNECT));
  
  if (Serial1.available())
  {
    val = Serial1.readString();
            
    if (val == "REQ_FW")
    {
      // Return the firmware version
      Serial1.println(FW_version);
    }
    else if (val == "BT_RESET")
    {
      // Reset the BT module
      Serial1.println("BT_reset");
      BT_reset();
    }
    else if (val.toInt() == 0) //start OTA
    {
      OTA_flag = 1; //Starts the OTA transfer routine next time the main loop is executed
      
      digitalWrite(RLED, LOW);
      digitalWrite(GLED, LOW);
      digitalWrite(BLED, HIGH);
    }
    else
    {
      devicestate = val.toInt();
      if(devicestate == 1){
        t_stdby = millis(); //Start standby timer
//        BT_reset();
        executeDeviceState(devicestate);
      }
      if(devicestate == 2){
        executeDeviceState(devicestate);
        tmp_timer = millis();
      }
    }
  }

  if (Serial.available())
  {
    val = Serial.readString();

    if (val == "REQ_FW")
    {
      // Return the firmware version
      Serial.println(FW_version);      
    }
    else
    {
      devicestate = val.toInt();
      if(devicestate == 2){
        USB_stream = true;
        executeDeviceState(devicestate);
      }
      if(devicestate == 1){
        executeDeviceState(devicestate);
      }
    }
  }
  else
    USB_stream = false;

  /*--------- Memory funtions------------*/
  switch(devicestate)
  {
  case 6 :
    //logging data
    //White. Stable.
    setLEDstate(1);
    flash_init();
    log_data_flag=true;
    devicestate=2;
    //      standby = 1;
    break; 
  case 7 :
    //clean flash
    setLEDstate(2);
    clean_flash();
    devicestate=2;
    current_address=0;
    setLEDstate(1);
    break;     
  case 8 :
    //dump data
    setLEDstate(2);
    for(long k=0; k<current_address; k+=256)
      {
        Serial.println("Mem block: ");
        Serial.println(k/256);
        dump_16(k);
      }
    devicestate=2;
    setLEDstate(1);
    break;
  case 9 :
    //erase flash
    setLEDstate(2);
    erase_flash();
    devicestate=1;
    current_address=0;
    setLEDstate(1);
    break;     
  default :
    // flash all 3 colors - reset device
    break;
  }
  /*---------------------------------------*/
}

void printData()
{
  char out[256] = {
    0      };
  sprintf(out, "%d;%d", devicestate, int(BatteryLevel));
//  Serial1.print(millis()),Serial1.print(";");
  Serial1.print(out),Serial1.print(";");
  Serial1.print(two_pi_mapping(compAngleX)),Serial1.print(";");
  Serial1.print(compAngleY),Serial1.print(";");
  Serial1.print(accX),Serial1.print(";");
//  Serial1.print(accY),Serial1.print(";");
//  Serial1.println(accZ);
  Serial1.println(gyroXangle);

  if(digitalRead(CHG_STAT) == LOW){
    Serial.print(out),Serial.print(";");
    Serial.print(two_pi_mapping(compAngleX)),Serial.print(";");
    Serial.print(compAngleY),Serial.print(";");
    Serial.print(accX),Serial.print(";");
    Serial.print(accY),Serial.print(";");
    Serial.print(accZ),Serial.print(";");
    Serial.print(gyroX),Serial.print(";");
    Serial.print(gyroY),Serial.print(";");
    Serial.println(gyroZ);
  }
  
    Serial.print(out),Serial.print(";");
    Serial.print(two_pi_mapping(compAngleX)),Serial.print(";");
    Serial.print(compAngleY),Serial.print(";");
    Serial.print(compAngleZ),Serial.print(";");
    Serial.print(accX),Serial.print(";");
    Serial.print(accY),Serial.print(";");
    Serial.print(accZ),Serial.print(";");
    Serial.print(gyroX),Serial.print(";");
    Serial.print(gyroY),Serial.print(";");
    Serial.print(gyroZ),Serial.print(";");
    Serial.print(magX),Serial.print(";");
    Serial.print(magY),Serial.print(";");
    Serial.print(magZ),Serial.println(";");
    
    /*for(int k; k<9; k++)
    {
      Serial.print(calibration_values[k]);
      Serial.print(";");
    }
    Serial.println(";");*/
   
  print_time = millis(); 
}

void BT_reset()
{
  pinMode(BT_RESET, OUTPUT);
  digitalWrite(BT_RESET, LOW);
  delay(10);
  pinMode(BT_RESET, INPUT);
  digitalWrite(BT_RESET, HIGH);
  Serial.println("BT reset");
  
  t_BTreset = millis();
}





