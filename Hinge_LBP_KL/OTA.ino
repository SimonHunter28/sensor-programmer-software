
/// Use watchdog to reset
void resetUsingWatchdog(boolean DEBUG)
{
  //wdt_disable();
  if (DEBUG) Serial1.print("REBOOTING");
  wdt_enable(WDTO_15MS);
  while (1) {}
}

//assume A and B are valid HEX chars [0-9A-F]
byte BYTEfromHEX(char MSB, char LSB)
{
  return (MSB >= 65 ? MSB - 55 : MSB - 48) * 16 + (LSB >= 65 ? LSB - 55 : LSB - 48);
}

//returns length of HEX data bytes if everything is valid
//returns 0 if any validation failed
int validateHEXData(void* data, byte length)
{
  //assuming 1 byte record length, 2 bytes address, 1 byte record type, N data bytes, 1 CRC byte
  char* input = (char*)data;
  //for(unsigned int i=0; i<length; i++) {Serial.print(input[i]);Serial.print(" ");}Serial.println("");
  //Serial.println(length);
  if ((length == 10) & (input[0] == '0') & (input[1] == '0') & (input[8] == 'F') & (input[9] == 'F')) return 255; //Mark as the end of file condition :00000001FF

  if (length < 12 || length % 2 != 0) return 0;
  uint16_t checksum = 0;
  //check valid HEX data and CRC
  for (byte i = 0; i < length; i++)
  {
    if (!((input[i] >= 48 && input[i] <= 57) || (input[i] >= 65 && input[i] <= 70))) //0-9,A-F
      return 0;
    if (i % 2 && i < length - 2) checksum += BYTEfromHEX(input[i - 1], input[i]);
  }
  checksum = (checksum ^ 0xFFFF) + 1;

  //Serial.print("final CRC:"); Serial.println((byte)checksum, HEX);
  //Serial.print("CRC byte:"); Serial.println(BYTEfromHEX(input[length - 2], input[length - 1]), HEX);

  //check CHECKSUM byte
  if (((byte)checksum) != BYTEfromHEX(input[length - 2], input[length - 1])) return 0;

  byte dataLength = BYTEfromHEX(input[0], input[1]); //length of actual HEX flash data (usually 16bytes)
  //calculate record length
  if (length != dataLength * 2 + 10) return 0; //add headers and checksum bytes (a total of 10 combined)

  return dataLength; //all validation OK!
}

byte readSerialLine(char* input, char endOfLineChar, byte maxLength, uint16_t timeout)
{
  byte inputLen = 0;
  Serial1.setTimeout(timeout);
  inputLen = Serial1.readBytesUntil(endOfLineChar, input, maxLength);
  input[inputLen] = 0; //null-terminate it
  Serial1.setTimeout(0);
  //Serial.println();
  return inputLen;
}


int OTA()
{
  int timeout = 3000;
  byte sendBuf[32];
  char input[64];
  uint8_t retries = 0;
  int addrExp = 0;
  int addrCur = 1;

  if (flash.initialize())
    Serial1.println("RDY");
  else
    Serial1.println("NRDY!");

  flash.blockErase32K(0);
  flash.writeBytes(0, "000000:", 7);//Set the header only when transfer is complete
  flash.writeByte(9, ':');
  int memAddr = 10;

  readSerialLine(input, '\n', 64, 2000);
  if (atoi(input) != 1) return 0; //Exit in case of an error
  else Serial1.println("ACK");

  int finished = 0;
  while (!finished) //Start the main data transmission process
  {
    int len = readSerialLine(input, '\n', 64, 2000);
    for (unsigned int i = 0; i < 63; i++) input[i] = input[i + 1]; //Get rid of the ":"
    len = len - 1;//Account for the newline terminator at the end
    int HexLength = validateHEXData(input, len);//Check, that there has been no erorrs on transmittion
    /*char FlashAddr[5];//Read the flash address for consistency checking
    for (unsigned int i = 0; i < 4; i++) FlashAddr[i] = input[(i + 2)]; FlashAddr[4] = '\0';*/

    if (HexLength > 0)
      addrCur = BYTEfromHEX(input[2], input[3]) * 256 + BYTEfromHEX(input[4], input[5]); //Get the current flash address

    if (HexLength == 255) break; //End of transmition condition

    //Serial.print("Exp addr: "); Serial.print(addrExp);
    //Serial.print(" Cur addr: "); Serial.println(addrCur);
    if ((HexLength > 0) && (addrExp == addrCur))//Check, that there is data to be written and the memory address is as expected
    {
      retries = 0;
      //Serial.print(" Data Bytes: "); Serial.println(HexLength);

      byte hexData[HexLength];
      for (unsigned int i = 0; i < HexLength; i++) hexData[i] = BYTEfromHEX(input[8 + i * 2], input[8 + i * 2 + 1]); //Extract the flash data from the ihex type line

      //Serial.print(" Binary Data: ");for(unsigned int i=0; i<HexLength/2; i++){Serial.print(hexData[i]);Serial.print(" ");}Serial.println("");

      flash.writeBytes(memAddr, hexData, HexLength);
      memAddr = memAddr + HexLength;
      Serial1.println("ACK");
      addrExp = addrExp + HexLength;
    }
    else if (retries < 3)
    {
      Serial1.println("NACK");
      retries++;
    }
    else//Error with transfer
    {
      //if((addrExp != addrCur))Serial.println("Memory missing");
      flash.blockErase32K(0);//erase memory
      return 0;
    }
  }

  OTA_flag = 0;//Clear the flag so the program does not enter OTA function again
  Serial1.println("EOF");//Mark that the file transferred successfully

  flash.writeByte(7, (memAddr - 10) >> 8); //Mark how many bytes have been written
  flash.writeByte(8, (memAddr - 10));
  flash.writeBytes(0, "TRXIMG:", 7);//Set the header so that the bootloader knows a valid image has been transferred
  //Read the memory back for debugging purposes
  /*int wdth = 16;
  int lgth = 100;
  char buf[11];
  flash.readBytes(0, buf, 10);
  buf[10] = '\0';
  Serial.write(buf);
  Serial.println("");
  for (unsigned int j = 0; j < lgth; j++)
  {
    for (unsigned int u = 0; u < wdth; u++)
    {
      Serial.print(flash.readByte(wdth * j + u + 10));
      Serial.print(' ');
    }
    Serial.println(' ');
  }*/
  resetUsingWatchdog(true);//Reset the device for the bootloader to do it's job
}

