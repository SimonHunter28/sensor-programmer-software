// This function read Nbytes bytes from I2C device at address Address.
// Put read bytes starting at register Register in the Data array.
void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.endTransmission();

  // Read Nbytes
  Wire.requestFrom(Address, Nbytes);
  uint8_t index = 0;
  while (Wire.available())
    Data[index++] = Wire.read();
}


// Write a byte (Data) in device (Address) at register (Register)
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.write(Data);
  Wire.endTransmission();
}

void setup() {

  pinMode(BT_CONNECT, INPUT);

  /*LED initialization*/
  int time_on = 400;
  int time_off = 400;

  pinMode(RLED, OUTPUT);
  pinMode(GLED, OUTPUT);
  pinMode(BLED, OUTPUT);
  SoftPWMBegin();

  SoftPWMSet(GLED, LED_OFF);//Don't forget the LEDs are inverted, 0 turns them on.
  SoftPWMSet(RLED, LED_OFF);//Don't forget the LEDs are inverted, 0 turns them on.
  SoftPWMSet(BLED, LED_OFF);//Don't forget the LEDs are inverted, 0 turns them on.

  SoftPWMSetFadeTime(GLED, time_on, time_off);
  SoftPWMSetFadeTime(RLED, time_on, time_off);
  SoftPWMSetFadeTime(BLED, time_on, time_off);

  /*Serial communication and I2C initialization*/
  Serial.begin(115200);//This is for the USB interface
  Serial1.begin(115200);//This is for the Bluetooth interface
  Wire.begin();

  /*Setup the IMU for data acquisition*/

  /*Wire.beginTransmission(MPU);
    Wire.write(0x6B);  // PWR_MGMT_1 register
    Wire.write(0);     // set to zero (wakes up the MPU-6050)
    Wire.endTransmission(true);*/

  time = millis(); // set current time;


  TWBR = ((F_CPU / 400000L) - 16) / 2; // Set I2C frequency to 400kHz

  /*i2cData[0] = 7; // Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
    i2cData[1] = 0x00; // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
    i2cData[2] = 0x00; // Set Gyro Full Scale Range to ±250deg/s
    i2cData[3] = 0x00; // Set Accelerometer Full Scale Range to ±2g
    while (i2cWrite(0x19, i2cData, 4, false)); // Write to all four registers at once
    while (i2cWrite(0x6B, 0x01, true)); // PLL with X axis gyroscope reference and disable sleep mode

    while (i2cRead(0x75, i2cData, 1));
    if (i2cData[0] != 0x68) { // Read "WHO_AM_I" register
    Serial.print(F("Error reading sensor"));
    while (1);
    }*/

  I2CwriteByte(MPU, 29, 0x06);
  // Set gyroscope low pass filter at 5Hz
  I2CwriteByte(MPU, 26, 0x06);
  // Configure gyroscope range
  I2CwriteByte(MPU, 27, 0x00);//Gyro at 250 DPS
  // Configure accelerometers range
  I2CwriteByte(MPU, 28, 0x00);//Accel scale 2G
  // Set by pass mode for the magnetometers
  I2CwriteByte(MPU, 0x37, 0x02);
  delay(100);
  I2CwriteByte(MAG, 0x0A, 0x16); // Request continuous magnetometer measurements in 16 bits

  delay(100); // Wait for sensor to stabilize


  //Set state to switched on;
  devicestate = 2;
  executeDeviceState(devicestate);


  /* Set comp and gyro starting angle */
  while (i2cRead(0x3B, i2cData, 6));
  accX = (i2cData[0] << 8) | i2cData[1];
  accY = (i2cData[2] << 8) | i2cData[3];
  accZ = (i2cData[4] << 8) | i2cData[5];

  roll  = atan(accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
  pitch = atan2(-accX, accZ) * RAD_TO_DEG;

  gyroXangle = pitch;
  gyroYangle = roll;
  compAngleX = pitch;
  compAngleY = roll;

  timer = micros();
  t_stdby = millis();
  print_time = millis();

  //Pre-fill the battery level array so that the device doesn't immediately go to low-battery state
  for (int i = 0; i < Array_size; i++) {
    b_array[i] = 90;
  }
  b_mean = 90;

  /*------------------------------Some memory bits--------------------------*/
  //flash_init();
  read_calibration();
  /*------------------------------Some memory bits--------------------------*/
}

