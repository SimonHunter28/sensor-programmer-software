
void setLEDstate(int state)
{
  switch(state)
  {    
    case 1 :
      // Green constant
      SoftPWMSet(RLED, LED_OFF);
      SoftPWMSet(GLED, LED_ON);
      SoftPWMSet(BLED, LED_OFF);
      break;
      
    case 2 :
      // Red flash
      SoftPWMSet(BLED, LED_OFF);
      SoftPWMSet(GLED, LED_OFF);
      if (time - flashtimer < 600)
      {
        SoftPWMSet(RLED, LED_ON);
      }
      else if (time - flashtimer < 1200)
      {
        SoftPWMSet(RLED, LED_OFF);
      }
      else
      {
        flashtimer = millis();
      }
      break;
      
    case 3 :    
      // Orange pulse
      if (time - flashtimer < 1000)
      {
        SoftPWMSetPercent(GLED, 80);
        SoftPWMSetPercent(RLED, 80);
        SoftPWMSetPercent(BLED, 100);
//        digitalWrite(GLED, LOW);
//        digitalWrite(RLED, LOW);
//        digitalWrite(BLED, HIGH);
      }
      else if (time - flashtimer < 1600)
      {
        SoftPWMSetPercent(GLED, 100);
        SoftPWMSetPercent(RLED, 100);
        SoftPWMSetPercent(BLED, 100);
//        digitalWrite(GLED, HIGH);
//        digitalWrite(RLED, HIGH);
//        digitalWrite(BLED, HIGH);
      }
      else
      {
        flashtimer = millis();
      }
      break;
      
    case 4 :
      // White ON
      SoftPWMSet(RLED, LED_ON);
      SoftPWMSet(GLED, LED_ON);
      SoftPWMSet(BLED, LED_ON);
      break;
            
    
    default :
      // LEDs off
      SoftPWMSetPercent(GLED, 100);
      SoftPWMSetPercent(RLED, 100);
      SoftPWMSetPercent(BLED, 100);
      break;
  }  
}

