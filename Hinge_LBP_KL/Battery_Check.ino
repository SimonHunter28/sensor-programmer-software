//check battery and charge state;
boolean CHARGE;
boolean CHARGE_COMPLETE, BATT_LOW;
float BatteryLevel; 
double minBatV = 86;
int maxBatV = 99; //The maximum BatteryLevel value after charging, with the USB plogged in
int pre_devicestate;

const int Array_size = 10;
float b_array[Array_size];
double b_mean;
int b_index;

void Battery_Check()
{
   BatteryLevel = 100*double(analogRead(BATT_LEV))/651;
   BatteryLevel = Battery_Smooth(BatteryLevel);  
   pre_devicestate = devicestate;
   
   if(digitalRead(CHG_STAT) == LOW && CHARGE_COMPLETE == false){ //Battery charging
     devicestate = 4;
     CHARGE = true;
   }
     
   if(CHARGE && digitalRead(CHG_STAT) == HIGH){ //charger unplugged, turn off
     CHARGE = false;
     devicestate = 1;
     pinMode(POWER_PIN, OUTPUT);
     digitalWrite(POWER_PIN, LOW);
   }
      
   if(CHARGE && BatteryLevel >= maxBatV){ // Battery charged
      devicestate = 5;
      CHARGE_COMPLETE = true;
   }

   if(BatteryLevel < minBatV && CHARGE == false){ //Set device state to standby if battery level is low and not charging
      devicestate = 3;
      t_stdby = millis();
   }    
 
 //if device state has changed apply new device state
  if(devicestate != pre_devicestate || devicestate == 3 || devicestate == 4)
    executeDeviceState(devicestate);
  
  //if device has been in standby for 5 mins, turn off
  if((millis() - t_stdby > 3E5) && (devicestate == 1 || devicestate == 3)){ 
  pinMode(POWER_PIN, OUTPUT);
  digitalWrite(POWER_PIN, LOW);
  }
   
  checktime = millis();
     
}

double Battery_Smooth(float BattLevel)
{
  b_mean = (b_mean*10 - b_array[b_index] + BattLevel)/10;
  b_array[b_index] = BattLevel;
  b_index ++;
  
  if(b_index == Array_size)
    b_index = 0;
   
  return b_mean;
}
