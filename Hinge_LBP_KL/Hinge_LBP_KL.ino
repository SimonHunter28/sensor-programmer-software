/* Firmware for PCB v6.1 running the MPU9250 IMU and RN42 bluetooth module on an ATmega32u4 16MHz microcontroller*/
/*Written by Simon & Karolis, April 2016, Hinge Health ltd.*/

String FW_version = "LBP_v2.5";

#include <Wire.h>
#include <SoftPWM.h>
#include <avr/wdt.h>

/*Memory stuff*/
#include <SPIFlash.h>
#include <SPI.h>
#include <EEPROM.h>
#define FLASH_SS      SS // and FLASH SS pin
char input = 0;
long lastPeriod = -1;
SPIFlash flash(FLASH_SS, 0x140);
long current_address = 0;
boolean log_data_flag = false;

/* Define some LED stuff*/
#define LED_ON 0 //Don't forget the LEDs are inverted, 0 turns them on.
#define LED_OFF 255

int RLED = A2 ;
int GLED = A0 ;
int BLED = A1 ;

int BATT_LEV = A4;
int CHG_STAT = A5;
int POWER_PIN = 7;
int BT_RESET = 6;
int BT_CONNECT = 4;

int OTA_flag = 0; //Add to the initialization of other global variables
int dev_status = 0;

/* IMU Data */
const int MPU = 0x68; // I2C address of the MPU-6050
const int MAG = 0x0C; // I2C address of the magnetometer

double accX, accY, accZ;
double gyroX, gyroY, gyroZ;//MPU-6050 variables
double magX, magY, magZ;//MPU-6050 variables
uint8_t i2cData[14]; // Buffer for I2C data
uint32_t timer;
int16_t tempRaw;
int calibration_values[12];

double AlignThresh = 10;
double gyroXangle, gyroYangle, gyroZangle; // Angle calculate using the gyro only
double compAngleX, compAngleY, compAngleZ; // Calculated angle using a complementary filter
double roll, pitch, yaw;
float tempPitch, tempPitch2, delta;
int filter = 30;

/* Device State*/
int devicestate = 1; //
int standby; //0: ACTIVE 1: DISABLED
double dt;
double t_stdby;
double t_BTreset;
double time;
double flashtimer;
double checktime;
double print_time;
double tmp_timer;
String val;
boolean USB_stream = false;

void loop()
{
  time = millis();

  //Bluetooth function turns on IMU
  check_serial();

  if (OTA_flag) //Check if OTA transfer initiated
    if (!OTA()) //Initiate OTA transfer, if there is a problem and returns a 0, clear the OTA flag
      OTA_flag = 0;

  // Battery check
  if (time - checktime > 500) {
    //prevents battery check when in USB streaming or when battery is low - prevents sensor bouncing out of low battery state
    if ((devicestate == 2 && USB_stream == true) || (devicestate == 3 && digitalRead(CHG_STAT) == HIGH)) {
      checktime = millis();
      executeDeviceState(devicestate);
    }
    else
      Battery_Check();
  }

  //Bluetooth module reset
  //  if(devicestate == 1 && (time - t_BTreset > 20000)){ //If in standby, reset BT module every 20 seconds
  //     BT_reset();
  //  }

  //Angle calculation
  if (standby == 0)
  {
    /* Update all the values */
    sensor_init();
    if (log_data_flag)
      log_data();

    roll  = atan(accY / sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
    pitch = atan2(-accX , accZ) * RAD_TO_DEG;
    double Bfy = magZ * sin(roll * DEG_TO_RAD) - magY * cos(roll * DEG_TO_RAD);
    double Bfx = magX * cos(pitch * DEG_TO_RAD) + magY * sin(pitch * DEG_TO_RAD) * sin(roll * DEG_TO_RAD) + magZ * sin(pitch * DEG_TO_RAD) * cos(roll * DEG_TO_RAD);
    yaw = atan2(-Bfy, Bfx) * RAD_TO_DEG;

    dt = (double)(micros() - timer) / 1000000; // Calculate delta time
    timer = micros();

    Impact_Filter();

    if (delta < filter) { //Only run and update the output angle if the accelerometer is stable to within the threshold given by 'filter'

      double gyroXrate = gyroX / 131.0; // Convert to deg/s
      double gyroYrate = gyroY / 131.0; // Convert to deg/s
      double gyroZrate = gyroZ / 131.0; // Convert to deg/s

      // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
      if ((pitch < -90 && compAngleX > 90) || (pitch > 90 && compAngleX < -90)) {
        compAngleX = pitch;
        gyroXangle = pitch;
      }

      gyroXangle += gyroXrate * dt; // Calculate gyro angle without any filter
      gyroYangle += gyroYrate * dt;
      gyroZangle += gyroZrate * dt;

      compAngleX = 0.93 * (compAngleX + gyroYrate * dt) + 0.07 * pitch; // Calculate the angle using a Complimentary filter
      compAngleY = 0.93 * (compAngleY + gyroXrate * dt) + 0.07 * roll;
      compAngleZ = 0.93 * (compAngleZ + gyroZrate * dt) + 0.07 * yaw;

      // Reset the gyro angle when it has drifted too much
      if (gyroXangle < -180 || gyroXangle > 180)
        gyroXangle = compAngleX;
      if (gyroYangle < -180 || gyroYangle > 180)
        gyroYangle = compAngleY;

    }
    if (time - print_time > 25)
      printData();
  }

}

void executeDeviceState(int state)
{
  switch (state)
  {
  case 1 :
    // Switched ON - not connected
    // Green. stable.
    setLEDstate(1);
    standby = 0;
    break;

  case 2 :
    // Streaming data
    // Green. stable.
    setLEDstate(1);
    standby = 0;
    timer = micros();
    sensor_init();
    compAngleX = atan2(-accX, accZ) * RAD_TO_DEG;
    break;

  case 3 :
    // Battery low
    // Red flash
    setLEDstate(2);
    standby = 1;
    break;

  case 4 :
    //charging
    //Orange blink
    setLEDstate(3);
    //    standby = 1; temporary measure
    break;

  case 5 :
    //charged
    //White. Stable.
    setLEDstate(4);
    //      standby = 1;
    break;

  default :
    break;
  }
}

float two_pi_mapping(float angle)
{
  if (angle < 0)
  {
    angle += 360;
  }
  return angle;
}

//Initialize the accelerometer and gyro values
void sensor_init()
{

  while (i2cRead(0x3B, i2cData, 14));
  accY = ((i2cData[0] << 8) | i2cData[1]);
  accX = ((i2cData[2] << 8) | i2cData[3]);
  accZ = ((i2cData[4] << 8) | i2cData[5]);
  tempRaw = (i2cData[6] << 8) | i2cData[7];
  gyroY = (i2cData[8] << 8) | i2cData[9];
  gyroX = (i2cData[10] << 8) | i2cData[11];
  gyroZ = (i2cData[12] << 8) | i2cData[13];

  unsigned char ST1;
  do
  {
    I2Cread(MAG, 0x02, 1, &ST1);
  }
  while (!(ST1 & 0x01));
  // Read magnetometer data
  uint8_t Mag[7];
  I2Cread(MAG, 0x03, 7, Mag);
  magX = -(Mag[1] << 8 | Mag[0]);
  magY = -(Mag[3] << 8 | Mag[2]);
  magZ = -(Mag[5] << 8 | Mag[4]);

  /*Calibration part*/
  accX = -calibration_values[0] + accX;
  accY = -calibration_values[1] + accY;
  accZ = calibration_values[2] - accZ;
  gyroX = -calibration_values[3] + gyroX;
  gyroY = -calibration_values[4] + gyroY;
  gyroZ = -calibration_values[5] + gyroZ;
  magX = calibration_values[6] + int(float(magX)*float(calibration_values[9])/float(10000));
  magY = calibration_values[7] + int(float(magY)*float(calibration_values[10])/float(10000));
  magZ = calibration_values[8] + int(float(magZ)*float(calibration_values[11])/float(10000));
}


