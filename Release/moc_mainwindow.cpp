/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Desktop/HingeProgrammer3/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[28];
    char stringdata[452];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 9), // "prog_trig"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 14), // "add_hinge_data"
QT_MOC_LITERAL(4, 37, 7), // "double*"
QT_MOC_LITERAL(5, 45, 13), // "add_prog_data"
QT_MOC_LITERAL(6, 59, 9), // "update_ui"
QT_MOC_LITERAL(7, 69, 9), // "cal_done1"
QT_MOC_LITERAL(8, 79, 10), // "cal_done1a"
QT_MOC_LITERAL(9, 90, 10), // "cal_done2a"
QT_MOC_LITERAL(10, 101, 11), // "cal_done1aa"
QT_MOC_LITERAL(11, 113, 8), // "all_done"
QT_MOC_LITERAL(12, 122, 6), // "finish"
QT_MOC_LITERAL(13, 129, 8), // "restartt"
QT_MOC_LITERAL(14, 138, 23), // "readyReadStandardOutput"
QT_MOC_LITERAL(15, 162, 22), // "readyReadStandardError"
QT_MOC_LITERAL(16, 185, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(17, 209, 23), // "on_pushButton_7_clicked"
QT_MOC_LITERAL(18, 233, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(19, 257, 23), // "on_pushButton_8_clicked"
QT_MOC_LITERAL(20, 281, 23), // "on_pushButton_9_clicked"
QT_MOC_LITERAL(21, 305, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(22, 329, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(23, 351, 24), // "on_pushButton_10_clicked"
QT_MOC_LITERAL(24, 376, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(25, 400, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(26, 424, 19), // "on_checkBox_toggled"
QT_MOC_LITERAL(27, 444, 7) // "checked"

    },
    "MainWindow\0prog_trig\0\0add_hinge_data\0"
    "double*\0add_prog_data\0update_ui\0"
    "cal_done1\0cal_done1a\0cal_done2a\0"
    "cal_done1aa\0all_done\0finish\0restartt\0"
    "readyReadStandardOutput\0readyReadStandardError\0"
    "on_pushButton_6_clicked\0on_pushButton_7_clicked\0"
    "on_pushButton_5_clicked\0on_pushButton_8_clicked\0"
    "on_pushButton_9_clicked\0on_pushButton_4_clicked\0"
    "on_pushButton_clicked\0on_pushButton_10_clicked\0"
    "on_pushButton_3_clicked\0on_pushButton_2_clicked\0"
    "on_checkBox_toggled\0checked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  134,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    3,  135,    2, 0x0a /* Public */,
       5,    3,  142,    2, 0x0a /* Public */,
       6,    1,  149,    2, 0x0a /* Public */,
       7,    0,  152,    2, 0x0a /* Public */,
       8,    0,  153,    2, 0x0a /* Public */,
       9,    0,  154,    2, 0x0a /* Public */,
      10,    0,  155,    2, 0x0a /* Public */,
      11,    0,  156,    2, 0x0a /* Public */,
      12,    0,  157,    2, 0x0a /* Public */,
      13,    0,  158,    2, 0x0a /* Public */,
      14,    0,  159,    2, 0x08 /* Private */,
      15,    0,  160,    2, 0x08 /* Private */,
      16,    0,  161,    2, 0x08 /* Private */,
      17,    0,  162,    2, 0x08 /* Private */,
      18,    0,  163,    2, 0x08 /* Private */,
      19,    0,  164,    2, 0x08 /* Private */,
      20,    0,  165,    2, 0x08 /* Private */,
      21,    0,  166,    2, 0x08 /* Private */,
      22,    0,  167,    2, 0x08 /* Private */,
      23,    0,  168,    2, 0x08 /* Private */,
      24,    0,  169,    2, 0x08 /* Private */,
      25,    0,  170,    2, 0x08 /* Private */,
      26,    1,  171,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4, QMetaType::Int,    2,    2,    2,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4, QMetaType::Int,    2,    2,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   27,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->prog_trig(); break;
        case 1: _t->add_hinge_data((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< double*(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->add_prog_data((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< double*(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->update_ui((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->cal_done1(); break;
        case 5: _t->cal_done1a(); break;
        case 6: _t->cal_done2a(); break;
        case 7: _t->cal_done1aa(); break;
        case 8: _t->all_done(); break;
        case 9: _t->finish(); break;
        case 10: _t->restartt(); break;
        case 11: _t->readyReadStandardOutput(); break;
        case 12: _t->readyReadStandardError(); break;
        case 13: _t->on_pushButton_6_clicked(); break;
        case 14: _t->on_pushButton_7_clicked(); break;
        case 15: _t->on_pushButton_5_clicked(); break;
        case 16: _t->on_pushButton_8_clicked(); break;
        case 17: _t->on_pushButton_9_clicked(); break;
        case 18: _t->on_pushButton_4_clicked(); break;
        case 19: _t->on_pushButton_clicked(); break;
        case 20: _t->on_pushButton_10_clicked(); break;
        case 21: _t->on_pushButton_3_clicked(); break;
        case 22: _t->on_pushButton_2_clicked(); break;
        case 23: _t->on_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::prog_trig)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::prog_trig()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
