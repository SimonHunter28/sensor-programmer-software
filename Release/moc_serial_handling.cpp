/****************************************************************************
** Meta object code from reading C++ file 'serial_handling.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Desktop/HingeProgrammer3/serial_handling.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'serial_handling.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_serial_handling_t {
    QByteArrayData data[8];
    char stringdata[80];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_serial_handling_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_serial_handling_t qt_meta_stringdata_serial_handling = {
    {
QT_MOC_LITERAL(0, 0, 15), // "serial_handling"
QT_MOC_LITERAL(1, 16, 14), // "data_available"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 7), // "double*"
QT_MOC_LITERAL(4, 40, 9), // "cal_done1"
QT_MOC_LITERAL(5, 50, 10), // "cal_done1a"
QT_MOC_LITERAL(6, 61, 9), // "cal_done2"
QT_MOC_LITERAL(7, 71, 8) // "get_line"

    },
    "serial_handling\0data_available\0\0double*\0"
    "cal_done1\0cal_done1a\0cal_done2\0get_line"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_serial_handling[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   39,    2, 0x06 /* Public */,
       4,    0,   46,    2, 0x06 /* Public */,
       5,    0,   47,    2, 0x06 /* Public */,
       6,    0,   48,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   49,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, 0x80000000 | 3, QMetaType::Int,    2,    2,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void serial_handling::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        serial_handling *_t = static_cast<serial_handling *>(_o);
        switch (_id) {
        case 0: _t->data_available((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< double*(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->cal_done1(); break;
        case 2: _t->cal_done1a(); break;
        case 3: _t->cal_done2(); break;
        case 4: _t->get_line(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (serial_handling::*_t)(QString , double * , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&serial_handling::data_available)) {
                *result = 0;
            }
        }
        {
            typedef void (serial_handling::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&serial_handling::cal_done1)) {
                *result = 1;
            }
        }
        {
            typedef void (serial_handling::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&serial_handling::cal_done1a)) {
                *result = 2;
            }
        }
        {
            typedef void (serial_handling::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&serial_handling::cal_done2)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject serial_handling::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_serial_handling.data,
      qt_meta_data_serial_handling,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *serial_handling::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *serial_handling::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_serial_handling.stringdata))
        return static_cast<void*>(const_cast< serial_handling*>(this));
    return QThread::qt_metacast(_clname);
}

int serial_handling::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void serial_handling::data_available(QString _t1, double * _t2, int _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void serial_handling::cal_done1()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void serial_handling::cal_done1a()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void serial_handling::cal_done2()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
